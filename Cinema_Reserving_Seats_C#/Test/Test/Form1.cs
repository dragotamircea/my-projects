﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    // Metodele pentru partea grafica
    public partial class Form1 : Form
    {
        int[] buttonCliked ;// butoanele apasate live , ce faciliteaza si deselectarea
        //verificarea butoane , fara apasarea butonului register
        bool[] verifySeats = new bool[24];
        Operatii op =new Operatii();
        String selectedName;
        String result;
        public Form1()
        {
            op=op.load();
            buttonCliked = new int[24];
            for (int i = 0; i < 24; i++)
            {
                buttonCliked[i] = 0;
            }
            for (int i = 0; i < 24; i++)
            {
                verifySeats[i] = false;
            }
            InitializeComponent();
            populateView();
            
        }

        public void populateView()
        {
            
            foreach (Person p in op.getList())
            {
                result = null;
                for(int i=0;i<p.getnrLoc().Length;i++)
                {
                    if (p.getLOC(i) == 1)
                        result = String.Concat(result, i.ToString()+ " ");
                }

                string[] row1 = new string[] { p.getName().ToString(), p.getAge().ToString(),result };
                this.getDataView1().Rows.Add(row1);
            }
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            int[] newButtonClicked = new int[24];

            for (int i = 0; i < 24; i++)
            {
                if(verifySeats[i]==true)
                newButtonClicked[i] = 1;
            }

            this.getDataView1().Rows.Clear();
            op.addPers(this.textBox1.Text.ToString(), Int32.Parse(this.textBox2.Text.ToString()), newButtonClicked);
            this.populateView();
            for(int i=0;i<24;i++)
            {
                buttonCliked[i] = 0;
            }

            for (int i = 0; i < 24; i++)
            {
                if (verifySeats[i] == true)
                    op.but[i] = verifySeats[i];
            }

            for (int i = 0; i < 24; i++)
            {
                verifySeats[i] = false;
            }

            op.save(op);
        }

 
        private void butoane_Click(object sender, EventArgs e)
        {
            Control ctrl = ((Control)sender);

            if (ctrl.BackColor == Color.Green)
            {
                buttonCliked[ctrl.TabIndex] = 1;
                ctrl.BackColor = Color.Red;
                verifySeats[ctrl.TabIndex] = true;
            }
            else
            {
                int yes = 0;
                for(int i=0;i<buttonCliked.Length;i++)
                {
                    if (buttonCliked[i] == 1 && ctrl.TabIndex == i)
                    {
                        yes = 1;
                        verifySeats[ctrl.TabIndex] = false;
                    }
                }
                if (yes == 1)
                ctrl.BackColor = Color.Green;
            }
            op.save(op);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            this.getDataView1().Rows.Clear();

            foreach (Person p in op.getList().ToList())
            {
                if(p.getName().Equals(selectedName))
                {
                    for (int i = 0; i < p.getnrLoc().Length; i++)
                    {
                        if (p.getLOC(i) == 1)
                        {
                            op.but[i] = false;
                            this.butoane[i].BackColor = Color.Green;
                        }
                    }
                    
                    op.getList().Remove(p);
                }
            }
            this.populateView();
            op.save(op);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.selectedName = this.getDataView1().Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        
    }
}
