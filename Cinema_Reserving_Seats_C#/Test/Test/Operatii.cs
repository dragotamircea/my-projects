﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    [Serializable]
    class Operatii
    {
        //Operatiile de adaugare in lista + serliazare / deserializare
        // but = validarea locurilor  mai apoi se salveaza

        public bool[] but;
        public List<Person> persoane;

        public Operatii()
        {
           persoane = new List<Person>();
            but = new bool[30];
            for(int i=0;i<29;i++)
            {
                but[i] = false;
            }
           
        }

        
        public void addPers(String name, int age,int[] nrLoc)
        {
            Person a = new Person(name, age, nrLoc);
            persoane.Add(a);
        }

        public List<Person> getList()
        {
            return persoane;
        }

        public void save(Object obj)
        { 

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, obj);
            stream.Close();
        }

        public Operatii load()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream("MyFile.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
             Operatii obj = (Operatii)formatter.Deserialize(stream);
            stream.Close();
            return obj;
            
        }
    }
}
