﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    //Clasa persoana 
    [Serializable]
    public class Person
    {
        private String name;
        private int age;
        private int[] nrLoc=null;
        public Person(String name,int age,int[] nrLoc)
        {
            this.name = name;
            this.age = age;
            this.nrLoc = nrLoc;
        }
        public String getName()
        {
            return name;
        }
        public int getAge()
        {
            return age;
        }
        public int[] getnrLoc()
        {
            return nrLoc;
        }
        public int getLOC(int i)
        {
            return nrLoc[i];
        }
    }
}
