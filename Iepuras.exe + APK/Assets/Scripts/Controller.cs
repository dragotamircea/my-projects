﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Controller : MonoBehaviour {

    private Rigidbody2D myRigidBody;
    private Animator myAnim;
    public float JumpForce = 500f;
    private float bunnyHurtTime = -1;
    private Collider2D myCollider;
    public Text scoreText;
    private float startTime;
    private float entryTime;
    private float entryTime2;
    private float entryTime3=0;
    private float deadtime;
    private int jumpsLeft = 2;
    public AudioSource jumpSfx;
    public AudioSource DeadSfx;
    public AudioSource CoinSfx;
    private int CarrotScore = 0;
    private int score2;
    public Text CarrotText;
    public GameObject jetpack;
    public GameObject score;
    public GameObject mushroom;
    public BoxCollider2D enemy;

    public float sec = 5f;
   
    // Use this for initialization
    void Start () {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();
        myCollider = GetComponent<Collider2D>();
       
        jetpack.SetActive(false);
        score.SetActive(false);
        startTime = Time.time;
        enemy.isTrigger = false;
        
            mushroom.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
        if (Time.time - entryTime2 >= 1)
            score.SetActive(false);

        if (entryTime3 != 0)
        {
            if (Time.time - entryTime3 >= 2.3)
                mushroom.SetActive(true);
            if (Time.time - entryTime3 >= 6)
             mushroom.SetActive(false);
            if (Time.time - entryTime3 >= 4)
                enemy.isTrigger = false;
            
        }



        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("Title");
        }
        if (bunnyHurtTime == -1)
        {
            if (Input.GetButtonUp("Jump") || Input.GetButtonUp("Fire1") && jumpsLeft > 0)
            {
                if (myRigidBody.velocity.y < 0)
                {
                    myRigidBody.velocity = Vector2.zero;

                }
                if (jumpsLeft == 1)
                {
                    myRigidBody.AddForce(transform.up * JumpForce* 0.75f);
                }
                else
                {
                    myRigidBody.AddForce(transform.up * JumpForce);
                }
                jumpsLeft--;

                jumpSfx.Play();
            }
            myAnim.SetFloat("Velocity", myRigidBody.velocity.y);
           
                scoreText.text = (Time.time - startTime).ToString("0.0");
           
           


        }
        else
        {

            if (Time.time > bunnyHurtTime + 2)
            {
                Application.LoadLevel(Application.loadedLevel);
                
                  }
        }
        CarrotText.text = " " + CarrotScore;
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        
        
            if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                foreach (PrefabsSpawner spawner in FindObjectsOfType<PrefabsSpawner>())
                {
                    spawner.enabled = false;
                }

                foreach (MoveLeft moveLefter in FindObjectsOfType<MoveLeft>())
                {
                    moveLefter.enabled = false;
                }

                bunnyHurtTime = Time.time;
                myAnim.SetBool("bunnyHurt", true);
                myRigidBody.velocity = Vector2.zero;
                myRigidBody.AddForce(transform.up * JumpForce);
                myCollider.enabled = false;
               
                DeadSfx.Play();
            }
        
        else if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            jumpsLeft = 2;
            jetpack.SetActive(false);
        }
    }
        void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "carrot")
        {

            Destroy(other.gameObject);
            CarrotScore++;

            if (CarrotScore%15 == 0)
            {
                entryTime = Time.time;
                jumpsLeft = 5;
                jetpack.SetActive(true);
            }
            if(CarrotScore % 20== 0)
            {
                entryTime2 = Time.time;
                startTime = startTime - 10;
                score.SetActive(true);
                
            }
            if (CarrotScore % 50 == 0)
            {
                entryTime3 = Time.time;

                enemy.isTrigger = true;

              }


            CoinSfx.Play();
        }

              
            }

         


             
        }
        
