import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;

public class Main {

	private static int i;
	protected static List<String> carti = new ArrayList(Arrays.asList("Ion Creanga: Amintiri din copilarie","Mihail Sadoveanu: Batagul","Ioan Slavici: Moara cu noroc","Fata din tren","Fluturi","Dupa ce te-am pierdut","Winnetou","Ultimul Mohican"));
	protected static int c=1;
	protected static StringBuilder data;
	protected static StringBuilder data1;
	protected static JTextPane textPane,rezervate;
	protected static JTextField f;
	
	public static void main(String[] args)
	{
		JFrame frame = new JFrame ("Biblioteca");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
		frame.setBounds(400,200,400,400);
		frame.getContentPane().setLayout(null);
		
		
		JPanel p= new JPanel();
		JLabel l = new JLabel("Carti:");
		JLabel r = new JLabel("Rezervate:");
		f = new JTextField();
		
		f.setBounds(100, 200, 90, 25);
		l.setBounds(160,5,100,70);
		r.setBounds(160,220,100,70);
		
		textPane = new JTextPane();
		textPane.setBounds(100, 80, 213, 70);
		
		rezervate = new JTextPane();
		rezervate.setBounds(100,280,213,50);
		
		JScrollPane jsp = new JScrollPane(textPane);
		JScrollPane jsp1 = new JScrollPane(rezervate);
		jsp.setBounds(100, 80, 213, 70);
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		jsp1.setBounds(100,280,240,50);
		jsp1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		JButton button = new JButton();
		button.setBounds(200,200, 90, 25);
		data=new StringBuilder();
		data1 = new StringBuilder();
		
		
		for(i=0;i<carti.size();i++)
		{
			int p1;
			p1=i+1;
			data.append(p1+"."+carti.get(i)+"\n");
		}
		
		textPane.setText(data.toString());
		
		textPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		textPane.setEditable(false);
		
		rezervate.setAlignmentX(Component.CENTER_ALIGNMENT);
		rezervate.setEditable(false);
		
		
		button.setText("Rezerva");
		
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				try {
					
					Controller.Iterare();
				} catch (Exceptie e1) {
					
					System.out.println(e1.getMessage());
					JOptionPane.showMessageDialog(frame, "Cartea nu exista!", "Eroare", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		
		frame.setVisible(true);
		frame.getContentPane().add(f);
		frame.getContentPane().add(l);
		frame.getContentPane().add(r);
		frame.getContentPane().add(jsp);
		frame.getContentPane().add(jsp1);
		frame.getContentPane().add(button);
	}
}
