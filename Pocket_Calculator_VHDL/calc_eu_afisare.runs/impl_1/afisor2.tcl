proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
set_msg_config  -ruleid {1}  -id {Place 30-568}  -new_severity {INFO} 
set_msg_config  -ruleid {2}  -id {Place 30-574}  -new_severity {INFO} 
set_msg_config  -ruleid {3}  -id {Place 30-574}  -string {{ERROR: [Place 30-574] Poor placement for routing between an IO pin and BUFG. If this sub optimal condition is acceptable for this design, you may use the CLOCK_DEDICATED_ROUTE constraint in the .xdc file to demote this message to a WARNING. However, the use of this override is highly discouraged. These examples can be used directly in the .xdc file to override this clock rule.
	< set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets swe_IBUF] >

	swe_IBUF_inst (IBUF.O) is locked to IOB_X0Y7
	 and swe_IBUF_BUFG_inst (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y2
Resolution: Poor placement of an IO pin and a BUFG has resulted in the router using a non-dedicated path between the two.  There are several things that could trigger this DRC, each of which can cause unpredictable clock insertion delays that result in poor timing.  This DRC could be caused by any of the following: (a) a clock port was placed on a pin that is not a CCIO-pin (b)the BUFG has not been placed in the same half of the device or SLR as the CCIO-pin (c) a single ended clock has been placed on the N-Side of a differential pair CCIO-pin.}}  -suppress 
set_msg_config  -ruleid {4}  -id {Place 30-99}  -string {{ERROR: [Place 30-99] Placer failed with error: 'IO Clock Placer failed'
Please review all ERROR, CRITICAL WARNING, and WARNING messages during placement to understand the cause for failure.}}  -suppress 
set_msg_config  -ruleid {5}  -id {Common 17-69}  -string {{ERROR: [Common 17-69] Command failed: Placer could not place all instances}}  -suppress 

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir C:/Users/mircea/calc_eu_afisare/calc_eu_afisare.cache/wt [current_project]
  set_property parent.project_path C:/Users/mircea/calc_eu_afisare/calc_eu_afisare.xpr [current_project]
  set_property ip_repo_paths c:/Users/mircea/calc_eu_afisare/calc_eu_afisare.cache/ip [current_project]
  set_property ip_output_repo c:/Users/mircea/calc_eu_afisare/calc_eu_afisare.cache/ip [current_project]
  add_files -quiet C:/Users/mircea/calc_eu_afisare/calc_eu_afisare.runs/synth_1/afisor2.dcp
  read_xdc C:/Users/mircea/calc_eu_afisare/calc_eu_afisare.srcs/constrs_1/new/constr.xdc
  link_design -top afisor2 -part xc7a35tcpg236-1
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force afisor2_opt.dcp
  report_drc -file afisor2_drc_opted.rpt
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  catch {write_hwdef -file afisor2.hwdef}
  place_design 
  write_checkpoint -force afisor2_placed.dcp
  report_io -file afisor2_io_placed.rpt
  report_utilization -file afisor2_utilization_placed.rpt -pb afisor2_utilization_placed.pb
  report_control_sets -verbose -file afisor2_control_sets_placed.rpt
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force afisor2_routed.dcp
  report_drc -file afisor2_drc_routed.rpt -pb afisor2_drc_routed.pb
  report_timing_summary -warn_on_violation -max_paths 10 -file afisor2_timing_summary_routed.rpt -rpx afisor2_timing_summary_routed.rpx
  report_power -file afisor2_power_routed.rpt -pb afisor2_power_summary_routed.pb
  report_route_status -file afisor2_route_status.rpt -pb afisor2_route_status.pb
  report_clock_utilization -file afisor2_clock_utilization_routed.rpt
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  catch { write_mem_info -force afisor2.mmi }
  write_bitstream -force afisor2.bit 
  catch { write_sysdef -hwdef afisor2.hwdef -bitfile afisor2.bit -meminfo afisor2.mmi -file afisor2.sysdef }
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

