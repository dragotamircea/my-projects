## This file is a general .xdc for the Basys3 rev B board
## To use it in a project:
## - uncomment the lines corresponding to used pins
## - rename the used ports (in each line, after get_ports) according to the top level signal names in the project
# Clock signal
set_property PACKAGE_PIN W5 [get_ports clk]							
	set_property IOSTANDARD LVCMOS33 [get_ports clk]
	create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports clk]
	
         set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets swr1_IBUF] 
       
             set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets swa_IBUF] 
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets swr2_IBUF] 
 set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets egal_IBUF] 
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets sws_IBUF] 

 
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets swim_IBUF] 


set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets swin_IBUF]

 
# Switches
set_property PACKAGE_PIN V17 [get_ports sel]					
	set_property IOSTANDARD LVCMOS33 [get_ports sel]
set_property PACKAGE_PIN V16 [get_ports clr]					
	set_property IOSTANDARD LVCMOS33 [get_ports clr]
set_property PACKAGE_PIN W16 [get_ports swr1]					
	set_property IOSTANDARD LVCMOS33 [get_ports swr1]
set_property PACKAGE_PIN W17 [get_ports swr2]					
	set_property IOSTANDARD LVCMOS33 [get_ports swr2]
set_property PACKAGE_PIN W15 [get_ports swa]					
	set_property IOSTANDARD LVCMOS33 [get_ports swa]
set_property PACKAGE_PIN V15 [get_ports sws]					
	set_property IOSTANDARD LVCMOS33 [get_ports sws]
set_property PACKAGE_PIN W14 [get_ports swin]					
	set_property IOSTANDARD LVCMOS33 [get_ports swin]
set_property PACKAGE_PIN W13 [get_ports swim]					
	set_property IOSTANDARD LVCMOS33 [get_ports swim]
#set_property PACKAGE_PIN V2 [get_ports sw15]					
#	set_property IOSTANDARD LVCMOS33 [get_ports sw15]
#set_property PACKAGE_PIN T3 [get_ports {x[9]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {x[9]}]
#set_property PACKAGE_PIN T2 [get_ports {x[10]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {x[10]}]
#set_property PACKAGE_PIN R3 [get_ports {x[11]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {x[11]}]
#set_property PACKAGE_PIN W2 [get_ports {x[12]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {x[12]}]
#set_property PACKAGE_PIN U1 [get_ports {x[13]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {x[13]}]
#set_property PACKAGE_PIN T1 [get_ports {x[14]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {x[14]}]
#set_property PACKAGE_PIN R2 [get_ports {x[15]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {x[15]}]
 

## virgulas
set_property PACKAGE_PIN U16 [get_ports virgula[0]]					
	set_property IOSTANDARD LVCMOS33 [get_ports virgula[0]]
set_property PACKAGE_PIN E19 [get_ports {virgula[1]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[1]}]
set_property PACKAGE_PIN U19 [get_ports {virgula[2]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[2]}]
set_property PACKAGE_PIN V19 [get_ports {virgula[3]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[3]}]
set_property PACKAGE_PIN W18 [get_ports {virgula[4]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[4]}]
set_property PACKAGE_PIN U15 [get_ports {virgula[5]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[5]}]
set_property PACKAGE_PIN U14 [get_ports {virgula[6]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[6]}]
set_property PACKAGE_PIN V14 [get_ports {virgula[7]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[7]}]
set_property PACKAGE_PIN V13 [get_ports {virgula[8]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[8]}]
set_property PACKAGE_PIN V3 [get_ports {virgula[9]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[9]}]
set_property PACKAGE_PIN W3 [get_ports {virgula[10]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[10]}]
set_property PACKAGE_PIN U3 [get_ports {virgula[11]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[11]}]
set_property PACKAGE_PIN P3 [get_ports {virgula[12]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[12]}]
set_property PACKAGE_PIN N3 [get_ports {virgula[13]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[13]}]
set_property PACKAGE_PIN P1 [get_ports {virgula[14]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[14]}]
set_property PACKAGE_PIN L1 [get_ports {virgula[15]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {virgula[15]}]
	
	
##7 segment display
set_property PACKAGE_PIN W7 [get_ports seg[0]]					
	set_property IOSTANDARD LVCMOS33 [get_ports seg[0]]
set_property PACKAGE_PIN W6 [get_ports seg[1]]					
	set_property IOSTANDARD LVCMOS33 [get_ports seg[1]]
set_property PACKAGE_PIN U8 [get_ports seg[2]]					
	set_property IOSTANDARD LVCMOS33 [get_ports seg[2]]
set_property PACKAGE_PIN V8 [get_ports seg[3]]					
	set_property IOSTANDARD LVCMOS33 [get_ports seg[3]]
set_property PACKAGE_PIN U5 [get_ports seg[4]]					
	set_property IOSTANDARD LVCMOS33 [get_ports seg[4]]
set_property PACKAGE_PIN V5 [get_ports seg[5]]					
	set_property IOSTANDARD LVCMOS33 [get_ports seg[5]]
set_property PACKAGE_PIN U7 [get_ports seg[6]]					
	set_property IOSTANDARD LVCMOS33 [get_ports seg[6]]

#set_property PACKAGE_PIN V7 [get_ports dp]							
#	set_property IOSTANDARD LVCMOS33 [get_ports dp]

set_property PACKAGE_PIN U2 [get_ports an[0]]					
	set_property IOSTANDARD LVCMOS33 [get_ports an[0]]
set_property PACKAGE_PIN U4 [get_ports an[1]]					
	set_property IOSTANDARD LVCMOS33 [get_ports an[1]]
set_property PACKAGE_PIN V4 [get_ports an[2]]					
	set_property IOSTANDARD LVCMOS33 [get_ports an[2]]
set_property PACKAGE_PIN W4 [get_ports an[3]]					
	set_property IOSTANDARD LVCMOS33 [get_ports an[3]]


##Buttons

set_property PACKAGE_PIN U18 [get_ports btnMijloc]						
	set_property IOSTANDARD LVCMOS33 [get_ports btnMijloc]
set_property PACKAGE_PIN T18 [get_ports btnUp]						
	set_property IOSTANDARD LVCMOS33 [get_ports btnUp]
set_property PACKAGE_PIN W19 [get_ports btnLeft]						
	set_property IOSTANDARD LVCMOS33 [get_ports btnLeft]
set_property PACKAGE_PIN T17 [get_ports btnRight]						
	set_property IOSTANDARD LVCMOS33 [get_ports btnRight]
set_property PACKAGE_PIN U17 [get_ports btnDown]						
	set_property IOSTANDARD LVCMOS33 [get_ports btnDown]