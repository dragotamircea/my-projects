package App;

public class Monom implements Comparable{
		
		private Integer exponent;
		private double coeficient;
		private boolean parcurs;
		private String rez=null;
		
		public Monom(double coeficient,int exponent)
		{
			this.coeficient = coeficient;
			this.exponent=exponent;
		}
		
		public void setExponent(Integer exponent)
		{
			this.exponent=exponent;
		}
		
		public Integer getExponent()
		{
			return exponent;
		}
		
		public void setCoeficient(double coeficient)
		{
			this.coeficient=coeficient;
		}
		
		public double getCoeficient()
		{
			return coeficient;
		}
		
		public void setParcurs(boolean parcurs)
		{
			this.parcurs=parcurs;
		}
		
		public boolean getParcurs()
		{
			return parcurs;
		}
		
		
		@Override
		public int compareTo(Object d) { // supraincarcam pt a putea sorta descrescator
			return ((Monom) d).exponent-exponent;
		}
		

		public String toString(int n) //Posibilitatile de afisare a unui monom in func de exponent si coef
		{
			
			if(n==1)
			{
				if(coeficient==0)
					rez= "";
				if(coeficient!=0)
					rez=coeficient+"x^"+exponent;
				if(exponent==0)
					rez=coeficient+"";
				if(exponent==0&& coeficient==0)
					rez="";
			}
			else
			{
				if(coeficient==0)
					rez= "";
				if(coeficient<0 && exponent!=0)
					rez = coeficient+"x^"+exponent;
				if(coeficient>0&& exponent!=0)
					rez ="+"+coeficient+"x^"+exponent;
				if(exponent==0 && coeficient>=0)
					rez="+"+coeficient+"";
				if(exponent==0 && coeficient<0)
					rez=coeficient+"";
				if(exponent==0 && coeficient==0)
					rez="";
		}
			return rez;
		}
		
		public boolean equals(Monom m){// supraincarcam equals pt a putea utilza assertEquals din JUnit
			if(m.getCoeficient()==coeficient && m.getExponent()==exponent)
				return true;
			else
				return false;
		}


}
