package App;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Operatii extends Polinom{
	//Clasa cu toate operatiile
	public static Monom rez;
	private static Polinom p1=new Polinom();
	private static Polinom p2=new Polinom();
	static int nr=0;
	
	//Setters si getter pt incapsulare
	public static void setP1(Polinom p)
	{
		p1=p;
	}
	
	public static void setP2(Polinom p)
	{
		p2=p;
	}
	
	public void addP1(Monom m)
	{
		p1.add(m);
	}
	
	public void addP2(Monom m)
	{
		p2.add(m);
	}
	
	public static Polinom getP1()
	{
		return p1;
	}
	
	public static Polinom getP2()
	{
		return p2;
	}
	
//Operatiile:
	
	public static Polinom adunare(Polinom a ,Polinom b)
	{
		Polinom rezultat = new Polinom();

		for(Monom m:a.getPolinom())
		{
			for(Monom n:b.getPolinom())
			{
				if(m.getExponent()==n.getExponent()) //adunarea se face intre termenii cu acelasi exponent
					{
						m.setParcurs(true); // parcurgere pt termenii ce nu au pereche
						n.setParcurs(true);
						rez=new Monom(m.getCoeficient()+n.getCoeficient(),m.getExponent());
						rezultat.add(rez);
						
					}
			}
		}

		for(Monom m:a.getPolinom())
		{
			if(m.getParcurs()==false) // cei fara pereche din polinomul 1 adaugati
				rezultat.add(m);
		}
		
		for(Monom m:b.getPolinom())
		{
			if(m.getParcurs()==false)// cei fara pereche din polinomul 2 adaugati
			{
					rezultat.add(m);
			}
	}
		
	Collections.sort(rezultat.getPolinom()); //sortare descrescatoare in func de exponent
			return rezultat;
	}
	
	public static Polinom scadere(Polinom a ,Polinom b)
	{
		Polinom rezultat = new Polinom();

		for(Monom m:a.getPolinom())
		{
			for(Monom n:b.getPolinom())
			{
				if(m.getExponent()==n.getExponent()) //scaderea se face intre termenii cu acelasi exponent
					{
						m.setParcurs(true);
						n.setParcurs(true);
						rez=new Monom(m.getCoeficient()-n.getCoeficient(),m.getExponent());
						rezultat.add(rez);
						
					}
			}
		}

		for(Monom m:a.getPolinom())
		{
			if(m.getParcurs()==false) //cei fara pereche din polinomul 1 adaugati
				rezultat.add(m);
		}
		
		for(Monom m:b.getPolinom())
		{
			if(m.getParcurs()==false)
			{
					m.setCoeficient(0-m.getCoeficient());//cei fara pereche din polinomul 2 adaugati cu minus in fata
					rezultat.add(m);
			}
	}
		
	Collections.sort(rezultat.getPolinom()); //sortare descrescatoare in func de exponent
			return rezultat;
	}
	
	
	
	public static String afisareRez(Polinom a) //Afisam din lista cu rezultatele 
	{
		Monom m = null;
		String rezultat =""; // Rezultatul afisat , concatenat
		int n=0;
		Iterator<Monom> i = a.getPolinom().iterator();
		while(i.hasNext())
		{
			n++;
			m = i.next();
			rezultat=rezultat+m.toString(n); // este concatenat in fucntie de metoda suprascris toString , in func de toate posibilitatile
	}
		a.getPolinom().clear();
		return rezultat;
	}
	
	
	public static Polinom derivare(Polinom a)
	{
		Polinom rezultat = new Polinom();
		Iterator<Monom> i = a.getPolinom().iterator();
		while(i.hasNext())
		{
			Monom m = i.next();
			
			m.setCoeficient(m.getCoeficient()*m.getExponent()); // derivare : produs intre coef si exp , apoi se scade din exp 1
			m.setExponent(m.getExponent()-1);
			
			rezultat.add(m);
			
		}
		return rezultat;
	}
	
	

	public static Polinom produs(Polinom a,Polinom b)
	{
		Polinom rezultatC= new Polinom();
		
		Monom r;
		Iterator<Monom> i = a.getPolinom().iterator(); // se face o parcurgere a primului polinom 
		while(i.hasNext())
		{
			Monom m = i.next();
			Iterator<Monom> j = b.getPolinom().iterator();// parcurgere a celui de al doilea polinom
			while(j.hasNext())
			{
				Monom n = j.next();
				
				r = new Monom(m.getCoeficient()*n.getCoeficient(),m.getExponent()+n.getExponent());//produsul intre fiecare monom
				rezultatC.add(r);
			}
		}
		return adunareInterna(rezultatC.getPolinom());//adunare interna pt a afisa rezultatul gata adunat
				
	}
	
	
	public static Polinom adunareInterna(List<Monom> a) // adunare interna 
	{
		Polinom rezultat = new Polinom();
		double e = a.get(0).getExponent();
		int poz = 0;
		for(int i=1;i<a.size();i++) // se face adunarea termenilor polinomului , pt o afisare cat mai buna
		{
			if(a.get(i).getExponent()==e)
			{
				a.get(poz).setCoeficient(a.get(poz).getCoeficient()+a.get(i).getCoeficient());
				a.get(i).setParcurs(true);
			}
			else
			{
				e=a.get(i).getExponent();
				poz = i;
			}
		}
		
		for(int i=0;i<a.size();i++)
		{
			if(a.get(i).getParcurs()==false)
			{
				rezultat.add(a.get(i));
			}
		}
		
		return rezultat;
		
	}
	
	
	
	public static Polinom integrare(Polinom p)
	{
		Polinom rezultat = new Polinom();
		Monom rez;
		Iterator<Monom> i = p.getPolinom().iterator();
		while(i.hasNext())
		{
			Monom m = i.next();
			
			double number = Math.round(m.getCoeficient()/(m.getExponent()+1) * 100); // se imparte coef la exp , si se afiseaza cu 2 zecimale
			number = number/100;
			
			rez=new Monom(number,m.getExponent()+1); // iar la exponent se adauga 1
			
			rezultat.add(rez);
		}
		p.curatare();
		return rezultat;
		
	}
		
	@SuppressWarnings("unchecked")
	public static Polinom impartire(Polinom p,Polinom p2)
	{
		Polinom s = new Polinom();
		
		//ordonam polinoamele descrescator
		Collections.sort(p.getPolinom());
		Collections.sort(p2.getPolinom());
		
			
		while(p.gradMax()>=p2.gradMax()&&!p.getPolinom().isEmpty()&&p.getCoeficient()!=0)// cu scaderi repetate
		{
		//impartirea polinomului mai mare la cel mai mic
		Iterator<Monom> i = p.getPolinom().iterator();
		Iterator<Monom> j = p2.getPolinom().iterator();
		
			Monom m = i.next();
			Monom n = j.next();
			
			double number = (m.getCoeficient()/n.getCoeficient());
			int number2 = m.getExponent()-n.getExponent();
			
			rez=new Monom(number,number2);
			
			s.add(rez);
			Polinom deScazut = new Polinom();
			//Produsl intre rezultat si impartitor

			deScazut= produs(s,p2);
	
			p=scadere(p,deScazut);
			
			p.curatare();
			
		}
		
		return s;
		}
		
}
	
	
