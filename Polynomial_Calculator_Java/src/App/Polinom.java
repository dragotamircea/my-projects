package App;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Polinom {

	private List<Monom> polinom;
	
	public Polinom()
	{
		polinom = new ArrayList<Monom>();
	}
	//getter
	public List<Monom> getPolinom()
	{
		return polinom;
	}
	
	public void setClear()// curatam lista de monoame
	{
		polinom.clear();
	}
	
	public void add(Monom a) // adaugam in lista de monoame
	{
		polinom.add(a);
	}
	
	public int gradMax()// returneaza gradul maxim
	{
		int max=0;
		for(Monom c : polinom)
		{
			
			if (c.getExponent()>max&&c.getCoeficient()!=0)
			{
				max=c.getExponent();
			}
		}
		
		return max;
	}
	
	public double getCoeficient()// coeficientul primului monom , dupa ce polinomul este sortat descrescator
	{
		Collections.sort(polinom);

		Monom m = polinom.get(0);
		
		return m.getCoeficient();
	}

	public double getExponent()// exponentul primului monom , dupa ce polinomul este sortat descrescator
	{
		Collections.sort(polinom);
		
		Monom m = polinom.get(0);
		
		
		return m.getExponent();
	}
	
	@SuppressWarnings("null")
	public void curatare() // Curatam polinomul de monoamele cu coeficient 0
	{
		int nr = 0;
		int j=0;
		int[] index = new int[100];
		
		for(Monom c : polinom)
		{
			
			if (c.getCoeficient()==0)
			{
				index[j]=polinom.indexOf(c);
				j++;
				
			}
			
		}
		
		//Toate elementele nenule
			for(int i=0;i<j;i++)
			{
			polinom.remove(index[i]);
			}
		
			if(j==1)
				polinom.clear();
		
	}
	
	@Override
	public boolean equals(Object o){ // supraincarcam equals pt a putea utilza assertEquals din JUnit
		Polinom poli=(Polinom)o;
		if(polinom.isEmpty() && poli.polinom.isEmpty())
			return true;
		if(polinom.size()==poli.polinom.size())
			for(int i=0;i<polinom.size();i++){
				if(!polinom.get(i).equals(poli.polinom.get(i)))
					return false;
			}
		else
			return false;
		return true;
	}
	
	
}
