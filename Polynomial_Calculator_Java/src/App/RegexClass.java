package App;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexClass {

		Polinom p;
		
		public static void regex(String input,Polinom poli){
			 int coef=1;
			 int putere=0;
			 Pattern p=Pattern.compile("(-?(\\d+)?x\\^?-?(\\d+)?|-?\\d+)"); // sablonul folosit , pentru a putea introduce sub forma ax^2+2x+2
			 Matcher m=p.matcher(input);
			 
			 while(m.find()){

				 try{
					 if(m.group().indexOf("x")!=0) // luam pe grupuri si introducem in monom
						 coef=Integer.parseInt(m.group().substring(0, m.group().indexOf('x')));// luam coef
							 if(m.group().indexOf("^")!=-1)
						 putere=Integer.parseInt(m.group().substring(m.group().indexOf("^")+1));// luam exp
							 else
									putere=1;
				 }
				 catch(StringIndexOutOfBoundsException e){
					 coef=Integer.parseInt(m.group());
					 putere=0;
				 }
				 finally{
					 Monom mon=new Monom(coef,putere);
					 poli.add(mon); // introducem in polinom
					 coef=1;
					 putere=0;// resetam variabila
				 }
			 }
			
		}
		
		
	}

