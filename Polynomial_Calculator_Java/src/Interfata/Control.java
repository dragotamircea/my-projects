
package Interfata;

import java.util.List;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import App.Operatii;
import App.RegexClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class Control {

    @FXML
    private JFXButton integrareButon;

    @FXML
    private JFXTextField text1;

    @FXML
    private JFXButton scadereButon;

    @FXML
    private JFXButton sumaButon;

    @FXML
    private JFXTextField text2;

    @FXML
    private JFXButton impartireButon;

    @FXML
    private JFXButton produsButon;

    @FXML
    private JFXTextArea rezText;
    
    @FXML
    private JFXTextField rezultat;

    @FXML
    private JFXButton derivareButon;

    @FXML
    void suma(ActionEvent event) {// apasare buton
    	Operatii.getP1().setClear(); //curatam inainte
    	Operatii.getP2().setClear();
    	RegexClass.regex(text1.getText(),Operatii.getP1());// Split cu Regex
    	RegexClass.regex(text2.getText(),Operatii.getP2());
    	Operatii.setP1(Operatii.adunareInterna(Operatii.getP1().getPolinom())); // Adunare interna a termenilor unui polinom 
    	Operatii.setP2(Operatii.adunareInterna(Operatii.getP2().getPolinom()));
    	rezultat.setText(Operatii.afisareRez(Operatii.adunare(Operatii.getP1(),Operatii.getP2())));
    	rezText.appendText("Adunare: "+rezultat.getText()+"\n");

    }

 

    @FXML
    void scadere(ActionEvent event) {
    	Operatii.getP1().setClear();
    	Operatii.getP2().setClear();
    	RegexClass.regex(text1.getText(),Operatii.getP1());
    	RegexClass.regex(text2.getText(),Operatii.getP2());
    	Operatii.setP1(Operatii.adunareInterna(Operatii.getP1().getPolinom())); // Adunare interna a termenilor unui polinom 
    	Operatii.setP2(Operatii.adunareInterna(Operatii.getP2().getPolinom()));
    	rezultat.setText(Operatii.afisareRez(Operatii.scadere(Operatii.getP1(),Operatii.getP2())));
		rezText.appendText("Scadere: "+rezultat.getText()+"\n");
    }


    @FXML
    void produs(ActionEvent event) {
    	Operatii.getP1().setClear();
    	Operatii.getP2().setClear();
    	RegexClass.regex(text1.getText(),Operatii.getP1());
    	RegexClass.regex(text2.getText(),Operatii.getP2());
    	Operatii.setP1(Operatii.adunareInterna(Operatii.getP1().getPolinom())); // Adunare interna a termenilor unui polinom 
    	Operatii.setP2(Operatii.adunareInterna(Operatii.getP2().getPolinom()));
    	rezultat.setText(Operatii.afisareRez(Operatii.produs(Operatii.getP1(),Operatii.getP2())));
		rezText.appendText("Produs: "+rezultat.getText()+"\n");
    }



    @FXML
    void impartire(ActionEvent event) {
    	Operatii.getP1().setClear();
    	Operatii.getP2().setClear();
    	RegexClass.regex(text1.getText(),Operatii.getP1());
    	RegexClass.regex(text2.getText(),Operatii.getP2());
    	Operatii.setP1(Operatii.adunareInterna(Operatii.getP1().getPolinom())); // Adunare interna a termenilor unui polinom 
    	Operatii.setP2(Operatii.adunareInterna(Operatii.getP2().getPolinom()));
    	if(text1.getText().equals(text2.getText()))
    	{
    		rezultat.setText("1.0");
    		rezText.appendText("Impartire: "+"1.0"+"\n");
    	}
    	else
    	{
    	rezultat.setText(Operatii.afisareRez(Operatii.impartire(Operatii.getP1(),Operatii.getP2())));
		rezText.appendText("Impartire: "+rezultat.getText()+"\n");
    	}
    }


    @FXML
    void derivare(ActionEvent event) {
    	Operatii.getP1().setClear();
    	Operatii.getP2().setClear();
    	RegexClass.regex(text1.getText(),Operatii.getP1());
    	rezultat.setText(Operatii.afisareRez(Operatii.derivare(Operatii.getP1())));
		rezText.appendText("Derivare: "+rezultat.getText()+"\n");
    }

 

    @FXML
    void integrare(ActionEvent event) {
    	Operatii.getP1().setClear();
    	Operatii.getP2().setClear();
    	RegexClass.regex(text1.getText(),Operatii.getP1());
    	rezultat.setText(Operatii.afisareRez(Operatii.integrare(Operatii.getP1())));
		rezText.appendText("Integrare: "+rezultat.getText()+"\n");
    }



}
