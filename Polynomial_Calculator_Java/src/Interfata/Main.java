package Interfata;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application{

	public static void main(String[] args)
	{
		Application.launch(args);
		
	}
	
	public void start(Stage primaryStage) throws Exception{
		Pane mainPane=(Pane)FXMLLoader.load(Main.class.getResource("Sample.fxml")); // se citeste fxmlul pt a initializa interfata
		primaryStage.setScene(new Scene(mainPane));
		primaryStage.setTitle("Calculator");
		primaryStage.show();
		
	}
	
	
}
