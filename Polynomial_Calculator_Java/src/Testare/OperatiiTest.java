package Testare;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;
import App.*;

public class OperatiiTest {
	

	@Test
	public void testAdunarePol() {
		Polinom a = new Polinom();
		RegexClass.regex("2x^2+2",a);
		Polinom b = new Polinom();
		RegexClass.regex("2x^2+2",b);
		Polinom exp = new Polinom();
		RegexClass.regex("4x^2+4",exp);
		
		Polinom suma = new Polinom();
		suma = Operatii.adunare(a, b);

		assertEquals(exp,suma);
		// daca suma este egal cu exp testul va fi trecut
	}
	
	@Test
	public void testScaderePol() {
		Polinom a = new Polinom();
		RegexClass.regex("4x^2+3",a);
		Polinom b = new Polinom();
		RegexClass.regex("2x^2+2",b);
		Polinom exp = new Polinom();
		RegexClass.regex("2x^2+1",exp);
		
		Polinom scadere = new Polinom();
		scadere = Operatii.scadere(a, b);

		assertEquals(exp,scadere);
		// daca cat este egal cu expectedS testul va fi trecut
	}

	@Test
	public void testInmultire() {
		Polinom a = new Polinom();
		RegexClass.regex("4x^2+2",a);
		Polinom b = new Polinom();
		RegexClass.regex("2x^2",b);
		Polinom exp = new Polinom();
		RegexClass.regex("8x^4+4x^2",exp);
		
		Polinom inmult = new Polinom();
		inmult = Operatii.produs(a, b);

		assertEquals(exp,inmult);
	}

	@Test
	public void testImpartire() {
		Polinom a = new Polinom();
		RegexClass.regex("4x^2",a);
		Polinom b = new Polinom();
		RegexClass.regex("2x^2",b);
		Polinom exp = new Polinom();
		RegexClass.regex("2",exp);
		
		Polinom inmult = new Polinom();
		inmult = Operatii.impartire(a, b);

		assertEquals(exp,inmult);
	}
	
	@Test
	public void testIntegrare() {
		Polinom a = new Polinom();
		RegexClass.regex("3x^2",a);

		Polinom exp = new Polinom();
		RegexClass.regex("x^3",exp);
		
		Polinom integ = new Polinom();
		integ = Operatii.integrare(a);

		assertEquals(exp,integ);
	}

	@Test
	public void testDerivare() {
		Polinom a = new Polinom();
		RegexClass.regex("3x^2",a);

		Polinom exp = new Polinom();
		RegexClass.regex("6x",exp);
		
		Polinom der = new Polinom();
		der = Operatii.derivare(a);

		assertEquals(exp,der);
	}

}
