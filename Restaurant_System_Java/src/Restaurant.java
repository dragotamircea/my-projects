import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class Restaurant {

	JFrame frame;
	private JTextField textBurger1;
	private JTextField textBurger2;
	private JTextField textBurger3;
	private JTextField textBurger4;
	private JTextField costBautura;
	private JTextField costMancare;
	private JTextField costLivrare;
	private JTextField textTotal;
	private JTextField textBon;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Restaurant window = new Restaurant();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Restaurant() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		final double[] i = new double[4];
		frame = new JFrame();
		frame.setBounds(100, 100, 1004, 633);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		
		JLabel lblNewLabel = new JLabel("Mircea's Management Systems");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 26));
		
		JLabel lblNewLabel_1 = new JLabel("BON");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 27));
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		
		JLabel lblNewLabel_5 = new JLabel("TOTAL");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		textTotal = new JTextField();
		textTotal.setFont(new Font("Dialog", Font.BOLD, 12));
		textTotal.setEditable(false);
		textTotal.setColumns(10);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_5)
					.addPreferredGap(ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
					.addComponent(textTotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(21))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(25)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_5)
						.addComponent(textTotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(25, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JButton totalButton = new JButton("TOTAL");
		totalButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double cTotal1 = Double.parseDouble(costMancare.getText()); 
				double cTotal2 = Double.parseDouble(costBautura.getText()); 
				double cTotal3 = Double.parseDouble(costLivrare.getText());
				double cTotal4 = cTotal1 + cTotal2 + cTotal3;
				String itotal = String.format("Lei %.2f", cTotal4);
				textTotal.setText(itotal);

			}
		});
		totalButton.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JButton btnNewButton_1 = new JButton("BON");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double cTotal1 = Double.parseDouble(costMancare.getText()); 
				double cTotal2 = Double.parseDouble(costBautura.getText()); 
				double cTotal3 = Double.parseDouble(costLivrare.getText());
				double cTotal4 = cTotal1 + cTotal2 + cTotal3;
				String itotal = String.format("BON : " + "\n\n"+" TOTAL : Lei %.2f \n\n Va multumim", cTotal4);
				
				textBon.setText(itotal);
	
				
			}
		});
		
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JButton btnNewButton_2 = new JButton("RESET");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				costLivrare.setText(null);
				costBautura.setText(null);
				costMancare.setText(null);
				textTotal.setText(null);
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JButton btnNewButton_3 = new JButton("EXIT");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnNewButton_3.setFont(new Font("Tahoma", Font.BOLD, 17));
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addGap(25)
					.addComponent(totalButton)
					.addGap(61)
					.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
					.addGap(58)
					.addComponent(btnNewButton_2)
					.addGap(63)
					.addComponent(btnNewButton_3)
					.addContainerGap(130, Short.MAX_VALUE))
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addGap(31)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
						.addComponent(totalButton)
						.addComponent(btnNewButton_1)
						.addComponent(btnNewButton_2)
						.addComponent(btnNewButton_3))
					.addContainerGap(32, Short.MAX_VALUE))
		);
		panel_3.setLayout(gl_panel_3);
		
		JLabel lblNewLabel_2 = new JLabel("Cost Bautura");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		costBautura = new JTextField();
		costBautura.setFont(new Font("Tahoma", Font.BOLD, 13));
		costBautura.setEditable(false);
		costBautura.setColumns(10);
		costBautura.setText("0");
		
		JLabel lblNewLabel_3 = new JLabel("Cost Mancare");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		costMancare = new JTextField();
		costMancare.setFont(new Font("Dialog", Font.BOLD, 12));
		costMancare.setEditable(false);
		costMancare.setColumns(10);
		costMancare.setText("0");
		
		JLabel lblNewLabel_4 = new JLabel("Cost Livrare");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		costLivrare = new JTextField();
		costLivrare.setFont(new Font("Dialog", Font.BOLD, 12));
		costLivrare.setEditable(false);
		costLivrare.setColumns(10);
		costLivrare.setText("0");
		
		final JCheckBox livrare = new JCheckBox("Livrare");
		livrare.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				double cost = 10.5;
				if(livrare.isSelected())
				{
					String slivrare = String.format("%.2f",cost);
					costLivrare.setText(slivrare);
					
				}
				else
					costLivrare.setText(null);
			}
		});
		livrare.setFont(new Font("Tahoma", Font.BOLD, 13));
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(20)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(livrare)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_2)
								.addComponent(lblNewLabel_3)
								.addComponent(lblNewLabel_4))
							.addGap(78)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addComponent(costLivrare, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(costBautura, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(costMancare, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(88, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(costBautura, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(costMancare, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_3))
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_4)
						.addComponent(costLivrare, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 4, Short.MAX_VALUE)
					.addComponent(livrare)
					.addContainerGap())
		);
		panel_2.setLayout(gl_panel_2);
		
		final JCheckBox burger1 = new JCheckBox("Burger de pui");
		burger1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				double cMancare = Double.parseDouble(costMancare.getText());
				double nrBurger1 = Double.parseDouble(textBurger1.getText());
				double cBurger1 = 5.5;
				
				if(burger1.isSelected())
				{
					i[0] = (nrBurger1 * cBurger1 ) + cMancare;
					String sMancare = String.format("%.2f", i[0]);
					costMancare.setText(sMancare);
				}
			}
		});
		burger1.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		final JCheckBox burger2 = new JCheckBox("Burger de vita");
		burger2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				double cMancare = Double.parseDouble(costMancare.getText());
				double nrBurger2 = Double.parseDouble(textBurger2.getText());
				double cBurger2 = 9.5;
				
				if(burger2.isSelected())
				{
					i[1] = (nrBurger2 * cBurger2 ) + cMancare;
					String sMancare = String.format("%.2f", i[1]);
					costMancare.setText(sMancare);
				}
			}
		});
		burger2.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		final JCheckBox burger3 = new JCheckBox("Cheeseburger");
		burger3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				double cMancare = Double.parseDouble(costMancare.getText());
				double nrBurger3 = Double.parseDouble(textBurger3.getText());
				double cBurger3 = 8;
				
				if(burger3.isSelected())
				{
					i[2] = (nrBurger3 * cBurger3 ) + cMancare;
					String sMancare = String.format("%.2f", i[2]);
					costMancare.setText(sMancare);
				}
			}
		});
		burger3.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JCheckBox burger4 = new JCheckBox("Burger Dragota");
		burger4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				double cMancare = Double.parseDouble(costMancare.getText());
				double nrBurger4 = Double.parseDouble(textBurger4.getText());
				double cBurger4 = 11.5;
				
				if(burger1.isSelected())
				{
					i[3] = (nrBurger4 * cBurger4 ) + cMancare;
					String sMancare = String.format("%.2f", i[3]);
					costMancare.setText(sMancare);
				}
			}
		});
		burger4.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		textBurger1 = new JTextField();
		textBurger1.setColumns(10);
		
		textBurger2 = new JTextField();
		textBurger2.setColumns(10);
		
		textBurger3 = new JTextField();
		textBurger3.setColumns(10);
		
		textBurger4 = new JTextField();
		textBurger4.setColumns(10);
		
		JLabel lblBautura = new JLabel("Bautura");
		lblBautura.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		final JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double Cola = 3.5;
				double Fanta = 3.5;
				double Sprite = 3.5;
				double Nestea = 4.0;
				double Cafea = 3.0;
				double Ursus = 3.2;
				if(comboBox.getSelectedItem().equals("Cola"))
				{
					String convert = String.format("%.2f",Cola);
					costBautura.setText(convert);
				}
				if(comboBox.getSelectedItem().equals("Fanta"))
				{
					String convert = String.format("%.2f",Fanta);
					costBautura.setText(convert);
				}
				if(comboBox.getSelectedItem().equals("Sprite"))
				{
					String convert = String.format("%.2f",Sprite);
					costBautura.setText(convert);
				}
				if(comboBox.getSelectedItem().equals("Nestea"))
				{
					String convert = String.format("%.2f",Nestea);
					costBautura.setText(convert);
				}
				if(comboBox.getSelectedItem().equals("Cafea"))
				{
					String convert = String.format("%.2f",Cafea);
					costBautura.setText(convert);
				}
				if(comboBox.getSelectedItem().equals("Ursus"))
				{
					String convert = String.format("%.2f",Ursus);
					costBautura.setText(convert);
				}
				if(comboBox.getSelectedItem().equals("Alege"))
				{
					
					costBautura.setText(null);
				}

			}
		});
		comboBox.setFont(new Font("Tahoma", Font.BOLD, 17));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Alege", "Cola", "Fanta", "Sprite", "Nestea", "Cafea", "Ursus"}));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(burger1)
						.addComponent(burger2)
						.addComponent(burger3)
						.addComponent(burger4)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(24)
							.addComponent(lblBautura, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
						.addComponent(textBurger1)
						.addComponent(textBurger2)
						.addComponent(textBurger3)
						.addComponent(textBurger4)
						.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(34, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(burger1)
						.addComponent(textBurger1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(burger2)
						.addComponent(textBurger2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(burger3)
						.addComponent(textBurger3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(burger4)
						.addComponent(textBurger4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBautura)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		
		JPanel panel_5 = new JPanel();
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(126)
							.addComponent(lblNewLabel))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(12)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(7)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(panel_5, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
										.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))))
					.addGap(24)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel_1)
							.addGap(214))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(lblNewLabel))
						.addComponent(lblNewLabel_1))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(64)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
								.addComponent(panel_5, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(85)
									.addComponent(panel, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)))
							.addGap(7)
							.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setIcon(new ImageIcon("C:\\Users\\mircea\\Desktop\\imgres.jpg"));
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon("C:\\Users\\mircea\\Desktop\\burger.jpg"));
		GroupLayout gl_panel_5 = new GroupLayout(panel_5);
		gl_panel_5.setHorizontalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
					.addComponent(lblNewLabel_6, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel_5.createSequentialGroup()
					.addContainerGap(75, Short.MAX_VALUE)
					.addComponent(lblNewLabel_7)
					.addGap(21))
		);
		gl_panel_5.setVerticalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
					.addComponent(lblNewLabel_6)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNewLabel_7)
					.addContainerGap(65, Short.MAX_VALUE))
		);
		panel_5.setLayout(gl_panel_5);
		
		textBon = new JTextField();
		textBon.setFont(new Font("Dialog", Font.BOLD, 15));
		textBon.setEditable(false);
		textBon.setColumns(10);
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4.setHorizontalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap()
					.addComponent(textBon, GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_4.setVerticalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap()
					.addComponent(textBon, GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
					.addContainerGap())
		);
		panel_4.setLayout(gl_panel_4);
		frame.getContentPane().setLayout(groupLayout);
	}
}
