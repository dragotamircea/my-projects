//
//  RWTViewController.m
//  Arduino_Servo
//
//  Created by Owen Lacy Brown on 5/21/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

#import "RWTViewController.h"
#import "BTDiscovery.h"
#import "BTService.h"

@interface RWTViewController ()
@property (strong, nonatomic) NSTimer *timerTXDelay;
@property (nonatomic) BOOL allowTX;
@end

@implementation RWTViewController

#pragma mark - Lifecycle




- (void)viewDidLoad {
  [super viewDidLoad];

  // Rotate slider to vertical position
//  UIView *superView = self.positionSlider.superview;
//  [self.positionSlider removeFromSuperview];
//  [self.positionSlider removeConstraints:self.view.constraints];
//  self.positionSlider.translatesAutoresizingMaskIntoConstraints = YES;
//  self.positionSlider.transform = CGAffineTransformMakeRotation(M_PI_2);
//  [superView addSubview:self.positionSlider];
  
  // Set thumb image on slider
  [self.positionSlider setThumbImage:[UIImage imageNamed:@"Bar"] forState:UIControlStateNormal];
    self.positionSlider.minimumValue = 0.0;
    self.positionSlider.maximumValue = 255.0;
    
  self.allowTX = YES;
  
  // Watch Bluetooth connection
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionChanged:) name:RWT_BLE_SERVICE_CHANGED_STATUS_NOTIFICATION object:nil];
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLeftRight:) name:READ_BLE_SERVICE_CHANGED_STATUS_NOTIFICATION object:nil];
    
  // Start the Bluetooth discovery process
  [BTDiscovery sharedInstance];
}

- (void)dealloc {
  
  [[NSNotificationCenter defaultCenter] removeObserver:self name:RWT_BLE_SERVICE_CHANGED_STATUS_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:READ_BLE_SERVICE_CHANGED_STATUS_NOTIFICATION object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  
  [self stopTimerTXDelay];
}

#pragma mark - IBActions

- (IBAction)positionSliderChanged:(UISlider *)sender {
  // Since the slider value range is from 0 to 180, it can be sent directly to the Arduino board
  
  [self sendPosition:(uint8_t)sender.value];
}

- (IBAction)clickForward:(id)sender {
    [[BTDiscovery sharedInstance].bleService sendChar:'f'];
}

- (IBAction)clickRight:(id)sender {
    [[BTDiscovery sharedInstance].bleService sendChar:'r'];
}

- (IBAction)clickBackward:(id)sender {
    [[BTDiscovery sharedInstance].bleService sendChar:'b'];
}

- (IBAction)clickLeft:(id)sender {
    [[BTDiscovery sharedInstance].bleService sendChar:'l'];
}
- (IBAction)clickBreak:(id)sender {
    [[BTDiscovery sharedInstance].bleService sendChar:'s'];
}

#pragma mark - Private

- (void)connectionChanged:(NSNotification *)notification {
  // Connection status changed. Indicate on GUI.
  BOOL isConnected = [(NSNumber *) (notification.userInfo)[@"isConnected"] boolValue];
  
  dispatch_async(dispatch_get_main_queue(), ^{
    // Set image based on connection status
    self.imgBluetoothStatus.image = isConnected ? [UIImage imageNamed:@"Bluetooth_Connected"]: [UIImage imageNamed:@"Bluetooth_Disconnected"];
    
    if (isConnected) {
      // Send current slider position
      [self sendPosition:(uint8_t)self.positionSlider.value];
    }
  });
}

- (void) updateLeftRight:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSArray *strings = (NSArray *) userInfo[@"strings"];
    NSString *left = strings[0];
    NSString *right = strings[1];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if([left isEqualToString:@"a"]) {
            [self.leftButton setBackgroundImage:[UIImage imageNamed:@"left_green_arrow"] forState:UIControlStateNormal];
        }else if([left isEqualToString:@"f"]) {
            [self.leftButton setBackgroundImage:[UIImage imageNamed:@"left_red_arrow"] forState:UIControlStateNormal];
        }
        
        if([right isEqualToString:@"a"]) {
            [self.rightButton setBackgroundImage:[UIImage imageNamed:@"right_green_arrow"] forState:UIControlStateNormal];
        }else if([right isEqualToString:@"f"]) {
            [self.rightButton setBackgroundImage:[UIImage imageNamed:@"right_red_arrow"] forState:UIControlStateNormal];
        }
    });
    
    
}

- (void)sendPosition:(uint8_t)position {
  // TODO: Add implementation
    static uint8_t lastPosition = 255;
    
    self.speedLabel.text = [[NSString alloc] initWithFormat:@"Viteza: %d",position];
    
    if (!self.allowTX) { // 1
        return;
    }
    
    // Validate value
    if (position == lastPosition) { // 2
        return;
    }
    
    else if ((position == 102) || (position == 98) || (position == 108) || (position == 114)) { // 3
        position = lastPosition;
    }
    
    // Send position to BLE Shield (if service exists and is connected)
    if ([BTDiscovery sharedInstance].bleService) { // 4
        [[BTDiscovery sharedInstance].bleService writePosition:position];
        lastPosition = position;
        
        // Start delay timer
        self.allowTX = NO;
        if (!self.timerTXDelay) { // 5
            self.timerTXDelay = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerTXDelayElapsed) userInfo:nil repeats:NO];
        }
    }
}

- (void)timerTXDelayElapsed {
  self.allowTX = YES;
  [self stopTimerTXDelay];
  
  // Send current slider position
  [self sendPosition:(uint8_t)self.positionSlider.value];
}

- (void)stopTimerTXDelay {
  if (!self.timerTXDelay) {
    return;
  }
  
  [self.timerTXDelay invalidate];
  self.timerTXDelay = nil;
}

@end
