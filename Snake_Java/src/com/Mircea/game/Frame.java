package com.Mircea.game;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

import com.Mircea.game.graphics.Screen;

public class Frame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Frame(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Snake");
		setResizable(false);
		setPreferredSize((new Dimension(1000, 800)));
	
		init();
	}
	
	public void init(){
		setLayout(new GridLayout(1,1,0,0));
		
		Screen s = new Screen();
		//JLabel lblScore = new JLabel("Score");
		
		add(s);
		pack();
		setLocationRelativeTo(null);
		
		setVisible(true);
	
	}
	
	public static void main(String[] args){
			new Frame();
	}
}
