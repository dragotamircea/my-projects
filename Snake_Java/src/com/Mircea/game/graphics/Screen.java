package com.Mircea.game.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.Mircea.game.Apple;
import com.Mircea.game.BodyPart;

public class Screen extends JPanel implements Runnable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int WIDTH = 800 , HEIGHT = 800;
	private Thread thread;
	private boolean running = false;
	private Random r;
	private BodyPart b;
	private ArrayList<BodyPart> snake;
	
	private Apple apple;
	private ArrayList<Apple> apples;
	private ArrayList<Apple> apples1;
	int p=0;
	JLabel lblScore = new JLabel("Score");
	private int xCoor = 10, yCoor = 10;
	private int size = 2;

	private int ticks = 0;
	
	public boolean right = true, left = false , up = false , down = false;
	
	private Key key;
	
	public Screen(){
		
		setFocusable(true);
		key = new Key();
		addKeyListener(key);
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		
		r= new Random();
		
		snake = new ArrayList<BodyPart>();
		apples = new ArrayList<Apple>();
		apples1 = new ArrayList<Apple>();
		start();
	}
	
	public void tick(){
		if(snake.size()==0)
		{
			b = new BodyPart(xCoor,yCoor,10);
			snake.add(b);
		}
		if(apples.size()==0)
		{
			int xCoor = r.nextInt(79);
			int yCoord = r.nextInt(79);
			
			apple =  new Apple(xCoor,yCoord,10);
			apples.add(apple);
		}
		
		
		if(apples1.size()==0)
		{
			int xCoor1 = r.nextInt(79);
			int yCoord1 = r.nextInt(79);
			
			apple =  new Apple(xCoor1,yCoord1,10);
			
			apples1.add(apple);
		}
		
		
		for(int i=0;i<apples.size();i++)
		{
			if(xCoor == apples.get(i).getxCoor()&& yCoor == apples.get(i).getyCoor())
			{
				size--;
				snake.remove(0);
				apples.remove(i);
				i--;
			}
		}

		for(int i=0;i<apples1.size();i++)
		{
			if(xCoor == apples1.get(i).getxCoor()&& yCoor == apples1.get(i).getyCoor())
			{
				size++;
				apples1.remove(i);
				i--;
			}
		}
		
		if(size<0)
		{
			stop();
		}
		
		for(int i = 0;i<snake.size();i++)
		{
			if(xCoor == snake.get(i).getxCoor() && yCoor == snake.get(i).getyCoor())
			{
				if(i != snake.size()-1)
				{
				
					stop();
					
				}
			}
		}
		
		if(xCoor <0 || xCoor >79 || yCoor<0 || yCoor >79)
			stop();
		
		ticks++;
		
		if(ticks > 400000){
			if(right) xCoor++;
			if(left) xCoor--;
			if(up) yCoor--;
			if(down) yCoor++;
			
			ticks = 0;
			
			b= new BodyPart(xCoor,yCoor,10);
			snake.add(b);
			
			if(snake.size()>size){
			snake.remove(0);

			}
		}

	}
	
	public void paint(Graphics g){
		g.clearRect(0, 0, WIDTH, HEIGHT);
	/*	for(int i = 0;i<WIDTH/10;i++)
			g.drawLine(i*10, 0,i*10, HEIGHT);
		for(int i = 0;i<HEIGHT/10;i++)
		{
			g.drawLine(0, i*10,WIDTH, i*10);
		}
*/
		
		for(int i=0;i<snake.size()-p;i++)
		{
			snake.get(i).draw(g);
		}
		
		for(int i=0;i<apples.size();i++)
		{
			apples.get(i).draw(g);
		}
		
		for(int i=0;i<apples1.size();i++)
		{
			apples1.get(i).draw(g);
		}
	}
	
	public void start(){
		running = true;
		thread = new Thread(this,"Game Loop");
		thread.start();
	}
	
	public void stop(){
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	public void run(){
		while(running){
			tick();
			repaint();
		}
	}

	public class Key implements KeyListener {
	

		@Override
		public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_RIGHT && !left){
			up = false;
			down = false;
			right = true;
		}
		
		if(key == KeyEvent.VK_LEFT && !right){
			up = false;
			down = false;
			left = true;
		}
		
		if(key == KeyEvent.VK_UP && !down){
			left = false;
			right = false;
			up = true;
		}
		
		if(key == KeyEvent.VK_DOWN && !up){
			left = false;
			right = false;
			down = true;
		}
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
		

	}
	
}
