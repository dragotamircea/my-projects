.386
.model flat,stdcall

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Sa se realizeze un program care executa functiile unui calculator de buzunar cu operatii aritmetice si
;logice (cu intregi pe 32 de biti) in zecimal si in hexazecimal. Operatiile trebuie sa fie implementate in
;functii.

; Operatii cerute: +, -, *, /, NOT, AND.

;Utilizatorul va introduce de la tastatura baza de numeratie (D sau H) si apoi va scrie expresiile pe care
;doreste sa le calculeze. Semnul „=” inseamna ca expresia trebuie sa fie calculata si rezultatul afisat pe
;ecran. Daca expresia incepe cu un operator, primul operand este considerat ca fiind egal cu ultimul
;rezultat obtinut. Daca anterior nu s-a calculat nici un rezultat, operandul se considera a fi 0. Programul
;va recunoaste comenzile num (schimbarea bazei de numeratie) si exit (terminarea programului). Pana
;la primirea comenzii exit, programul va rula in continuu. O expresie contine maxim 4 operanzi (3
;operatii). Inmultirea si impartirea sunt prioritare, nu se folosesc paranteze.
;Exemplu:
;> Introduceti baza de numeratie:
;> D
;> Introduceti o expresie:
;> 2+3-2*2=1
;> Introduceti o expresie:
;> +25=26
;> num
;> Introduceti baza de numeratie:
;> H
;> Introduceti o expresie:
;> 1A XOR B=11
;> Introduceti o expresie:
;> exit

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

includelib msvcrt.lib

extern exit : proc
extern scanf : proc
extern printf : proc

public start 
.data
mesajBaza db "Introduceti baza de numeratie " , 0
mesajExpr db "Introduceti o expresie " , 0 

bazaIntrd db ?
exprIntrd db 44 dup(0)

formatExpr db "%s" , 0

formatd db "%d" , 10, 0
formath db "%X" , 10,0

siroperatii db 4 dup (0)
sirnumere dd 5 dup (0)

index dd 0
index2 dd 0



.code
start:
	
	push offset mesajBaza			;Printare mesaj intorducere baza
	call printf						;
	add esp , 4						;
	
	push offset bazaIntrd			;salvare in memorie a bazei introduse 
	push offset formatExpr			;
	call scanf						;
	add esp , 8						;
	
	mov eax,0
	mov [sirnumere],eax
	
	expresie :
	
	mov [sirnumere+5],0
	
	push offset mesajExpr			;printare mesaj introducere expresie
	call printf						;
	add esp , 4						;
	
	push offset exprIntrd			;salvare in memorie a expresiei
	push offset formatExpr			;
	call scanf						;	 
	add esp , 8						;
	
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;cautam cuvinte cheie iesire program(exit) sau schimbare baza de numeratie (num)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Verificare expresie num
	
	
	mov al , [exprIntrd]            ;introducem primul caracter din memorie a expresiei inroduse 
	
	cmp al,'n'						;verificam prima litera este n pt num
	jne saripeste1					;daca este egala continuam verificarea	
	
	mov al , [exprIntrd+1] 			;incarcam acumulatorul cu urmatorul caracter din expresie 
	
	cmp eax,'u'						;
	jne saripeste1					;
	
	mov al , [exprIntrd+2] 			;incarcam acumulatorul cu urmatorul caracter din expresie 
	
	cmp al,'m'						; daca indeplineste toate conditiile se face sartitura la introducere baza 
	je start						;	
		
	saripeste1:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Verificare expresie exit
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	mov al , [exprIntrd]            ;introducem primul caracter din memorie a expresiei inroduse 
	
	cmp al,'e'						;verificam daca prima litera este e pt exit
	jne saripeste					;daca este egala continuam verificarea 
	
	mov al , [exprIntrd+1] 			;incarcam acumulatorul cu urmatorul caracter din expresie 
									;
	cmp al,'x'						;verificam umatoarele litere din expresie
	jne saripeste					;
	
	mov al , [exprIntrd+2]			;incarcam acumulatorul cu urmatorul caracter din expresie 
	
	cmp al,'i'						;
	jne saripeste					;
	
	mov al , [exprIntrd+3] 			;
	
	cmp al,'t'						;verificam ultimul caracter
	je exitp						;in caz de egalitate se face saritura la exit
	
	saripeste:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Verificam baza pt conversia numerelor
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	mov al,[bazaIntrd]				;incarcam baza 
	
	cmp al,'D' 						;verificam daca este baza 10
	jne nuZecimal					;daca verifica conditia continuam , daca nu sarim peste
	
	mov ecx,0
	mov [index],ecx					;initializam index si index2 pt a pregati programul pt o noua parcurgere
	mov [index2],ecx				;
	mov ecx,44						;initializam ecx contorul pentru structura repetitiva
	mov eax,0			 			;initializam eax
	
	loop_D:
	
	mov edx,[index2]
	xor ebx,ebx  					;initializam ebx
	mov bl,exprIntrd[edx]			;luam primul caracter din sir 
	inc edx							;marim contorul
	mov [index2],edx				;salvam in memorie valoarea la care am ramas cu verificarea 
	
	cmp bl,'+'						;verificam sirul pentru operatia +
	jne nupls						;sarim in cazul in care nu intalnim semnul +
	cmp eax,0
	jne nzplus						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzplus:							;
	mov ebx,[index];				;daca intalnim semnul plus scoatem din memorie indexul la care am ramas in noile sire (numere si operatii)
	mov [sirnumere+4*ebx],eax		;salvam un numar in sirul de numere
	mov [siroperatii+ebx],'+'		;salvam operatie in sirul cu operatii 
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nupls:
	
	cmp bl,'-'
	jne numin
	
	cmp eax,0
	jne nzminus						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzminus:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'-'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	numin:
		
	cmp bl,'*'
	jne nuori
	
	cmp eax,0
	jne nzori						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzori:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'*'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nuori:
	
	jmp sariPestei
	
	loop_Di:
	jmp loop_D
	sariPestei:
	
	cmp bl,'/'
	jne nudiv
	
	cmp eax,0
	jne nzdiv						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzdiv:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'/'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nudiv:
	
	cmp bl,'='						;verficam semnul = pt iesire program
	jne nuegl						
	mov ebx,[index];				
	mov [sirnumere+4*ebx],eax		;salvam ultimul numar din sirul initial
	xor eax,eax  					;initializam eax
	inc ebx							;
	mov [index],ebx					;
	mov ecx,1						;fortam contorul ecx sa iasa din bucla
	nuegl:
	
	cmp bl,'0'						;convertim numerele
	jb nuNumar						
	cmp bl,'9'						;verificam daca se incadreaza intre 0-9
	ja nuNumar
	
	mov edx,10						;inmultim cu 10 pt baza 10
	mul edx								
	sub bl,'0'
	add eax,ebx
	nuNumar:						

	loop loop_Di
	
	nuZecimal:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	cmp al,'H' 						;verificam daca este baza 16
	jne nuHexa						;daca verifica conditia continuam , daca nu sarimp peste
	
	mov ecx,0
	mov [index],ecx					;initializam index si index2 pt a pregati programul pt o noua parcurgere
	mov [index2],ecx					;
	mov ecx,44
	mov eax,0 			    		;initializam eax
	
	loop_H:
	
	mov edx,[index2]
	xor ebx,ebx  					;initializam ebx
	mov bl,exprIntrd[edx]			;luam un caracter din sir 
	inc edx							;marim contorul
	mov [index2],edx				;salvam in memorie valoarea la care am ramas cu verificarea 
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	cmp bl,'A'
	jne nuAnd
	;mov bl,exprIntrd[edx]			
	cmp exprIntrd[edx],'N'			;verificam urmatorul caracter din sir 
	jne nuAnd
	mov bl,exprIntrd[edx+1]			;luam un caracter din sir 
	cmp bl,'D'
	jne nuAnd
	
	add edx,2							;marim contorul
	mov [index2],edx				;salvam in memorie valoarea la care am ramas cu verificarea 
	
	cmp eax,0
	jne nzand						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzand:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'&'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nuAnd:
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	cmp bl,'N'
	jne nuNot
	mov bl,exprIntrd[edx]			;luam un caracter din sir 
	cmp bl,'O'
	jne nuNot
	mov bl,exprIntrd[edx+1]			;luam un caracter din sir 
	cmp bl,'T'
	jne nuNot
	
	inc edx							;marim contorul
	mov [index2],edx				;salvam in memorie valoarea la care am ramas cu verificarea 
	
	cmp eax,0
	jne nznot						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]			;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nznot:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'!'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nuNot:
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	
	
	cmp bl,'+'						;verificam sirul pentru operatia +
	jne nuplss						;sarim in cazul in care nu intalnim semnul +
	
	cmp eax,0
	jne nzplush						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzplush:	
	
	mov ebx,[index];				;daca intalnim semnul plus scoatem din memorie indexul la care am ramas in noile sire (numere si operatii)
	mov [sirnumere+4*ebx],eax		;salvam un numar in sirul de numere
	mov [siroperatii+ebx],'+'		;salvam operatie in sirul cu operatii 
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nuplss:
	
	cmp bl,'-'
	jne numinn
	
	cmp eax,0
	jne nzminush						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzminush:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'-'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	numinn:
		
	cmp bl,'*'
	jne nuorii
	
	cmp eax,0
	jne nzorih						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzorih:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'*'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nuorii:
	

	cmp bl,'/'
	jne nudivv
	cmp eax,0
	jne nzdivh						;Tratam cazul in care introducem semnul primadata 
	mov eax,[sirnumere]				;Salvam in eax rezultatul de la operatia anterioara(initial 0)
	nzdivh:	
	
	mov ebx,[index];
	mov [sirnumere+4*ebx],eax
	mov [siroperatii+ebx],'/'
	xor eax,eax  					;initializam eax
	inc ebx
	mov [index],ebx
	nudivv:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;extindem bucla loop
	jmp sariPesteH
	loop_Hi:
	jmp loop_H
	sariPesteH:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	cmp bl,'='						;verficam semnul = pt iesire program
	jne nuegll						
	mov ebx,[index];				
	mov [sirnumere+4*ebx],eax		;salvam ultimul numar din sirul initial
	xor eax,eax  					;initializam eax
	inc ebx							;
	mov [index],ebx					;
	mov ecx,1						;fortam contorul ecx sa iasa din bucla
	nuegll:
	
	cmp bl,'0'						;convertim numerele
	jb nuNumarr						
	cmp bl,'9'						;verificam daca se incadreaza intre 0-9
	ja nuNumarr
	
	mov edx,16						;inmultim cu 16 pt baza 16
	mul edx								
	sub bl,'0'
	add eax,ebx
	nuNumarr:
		
	cmp bl,'A'						;convertim numerele
	jb nuhex						
	cmp bl,'F'						;verificam daca se incadreaza intre A-F
	ja nuhex
	
	mov edx,16						;inmultim cu 16 pt baza 16
	mul edx								
	sub bl,'A'
	add bl,10
	add eax,ebx
	nuhex:

	loop loop_Hi

	nuHexa:
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Numerele au fost introduse in vectorul sirnumere
;Operatiile au fost introduse in vectorul siroperatii (!-NOT , &-AND , + , - , * , /)
;Tratam operatiile
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	jmp saripest
	
	siftn:
	mov ecx,4
	mov ebx,edx
	sub ecx,ebx
	siftnumar:
	mov eax,sirnumere[ebx+4]
	mov sirnumere[ebx],eax
	add ebx,4
	loop siftnumar
	
	
	sifts:
	mov ecx,4
	sub ecx,edx
	siftsemn:
	mov al,siroperatii[edx+1]
	mov siroperatii[edx],al
	inc edx
	loop siftsemn
	
	saripest:
	
	mov ecx,4
	mov edx,0
	loop_not:							;
	
	cmp siroperatii[edx],'!'			;
	jne sarinot							;
	mov eax,sirnumere[4*edx+4]      	;
	not eax
	mov sirnumere[4*edx+4],eax
	jmp siftn
	sarinot:
	
	inc edx
	loop loop_not
	
	mov ecx,4
	mov edx,0
	loop_and:							;
	
	cmp siroperatii[edx],'&'			;
	jne sariand							;
	mov eax,sirnumere[4*edx]      		;
	mov ebx,sirnumere[4*edx+4]
	and eax,ebx
	mov sirnumere[4*edx+4],eax
	jmp siftn
	sariand:
	
	inc edx
	loop loop_and
	
	mov ecx,4
	mov edx,0
	loop_muldiv:
	
	cmp siroperatii[edx],'*'			;
	jne sariori							;
	mov eax,sirnumere[4*edx]      		;
	mov ebx,sirnumere[4*edx+4]
	mov [index],edx
	mul ebx
	mov edx,[index]
	mov sirnumere[4*edx+4],eax
	jmp siftn
	sariori:
	
	cmp siroperatii[edx],'/'			;
	jne saridiv							;
	mov eax,sirnumere[4*edx]      		;
	mov ebx,sirnumere[4*edx+4]
	mov [index],edx
	mov edx,0
	div ebx
	mov edx,[index]
	mov sirnumere[4*edx+4],eax
	jmp siftn
	saridiv:
	
	inc edx
	loop loop_muldiv
	
	mov ecx,4
	mov edx,0
	loop_addsub:
	
	cmp siroperatii[edx],'+'			;
	jne sariadd							;
	mov eax,sirnumere[4*edx]      		;
	mov ebx,sirnumere[4*edx+4]
	add eax,ebx
	mov sirnumere[4*edx+4],eax
	jmp siftn
	sariadd:
	
	cmp siroperatii[edx],'-'			;
	jne sarisbb							;
	mov eax,sirnumere[4*edx]      		;
	mov ebx,sirnumere[4*edx+4]
	sbb eax,ebx
	mov sirnumere[4*edx+4],eax
	jmp siftn
	sarisbb:
	
	inc edx
	
	
	loop loop_addsub
	
	
	mov al,[bazaIntrd]
	cmp al,'D'
	jne saribzd
	push sirnumere
	push offset formatd
	call printf
	add ESP, 8 ;curatam 2 argumente de pe stiva
	
	saribzd:
	
	cmp al,'H'
	jne saribzH
	push sirnumere
	push offset formath
	call printf
	add ESP, 8 ;curatam 2 argumente de pe stiva
	
	saribzH:
	
	jmp expresie ;repetam pana la comanda exit
	
	exitp:
	
	push 0 
	call exit
end start