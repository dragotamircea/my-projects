.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
extern malloc: proc
extern memset: proc

includelib canvas.lib
extern BeginDrawing: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
;aici declaram date
window_title DB "Tic Tac Toe",0
area_width EQU 640
area_height EQU 480
area DD 0
nr dd 2
e_x dd 0
e_0 dd 0
litera_noua dd 'A',0
litera dd 'A',0
litera1 dd 'A',0
litera2 dd 'A',0
litera3 dd 'A',0
litera4 dd 'A',0
litera5 dd 'A',0
litera6 dd 'A',0
litera7 dd 'A',0
litera8 dd 'A',0
counter Dw 0 ; numara
a dd 2
b dd 2
c1 dd 2
d dd 2
e dd 2
f dd 2
g dd 2
h dd 2
i dd 2

arg1 EQU 8
arg2 EQU 12
arg3 EQU 16
arg4 EQU 20

symbol_width EQU 40
symbol_height EQU 40

symbol_width1 equ 10
symbol_height1 equ 20

include digits.inc
include letters.inc
include letters1.inc
.code

; procedura make_text afiseaza o litera sau o cifra la coordonatele date
; arg1 - simbolul de afisat (litera sau cifra)
; arg2 - pointer la vectorul de pixeli
; arg3 - pos_x
; arg4 - pos_y
make_text proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit
	cmp eax, 'Z'
	jg make_digit
	sub eax, 'A'
	lea esi, letters
	jmp draw_text
make_digit:
	cmp eax, '0'
	jl make_space
	cmp eax, '9'
	jg make_space
	sub eax, '0'
	lea esi, digits
	jmp draw_text
make_space:	
	mov eax, 26 ; de la 0 pana la 25 sunt litere, 26 e space
	lea esi, letters
	
draw_text:
	mov ebx, symbol_width
	mul ebx
	mov ebx, symbol_height
	mul ebx
	add esi, eax
	mov ecx, symbol_height
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov dword ptr [edi], 0
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov dword ptr [edi], 005262ddh
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_text endp

; un macro ca sa apelam mai usor desenarea simbolului
make_text_macro macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_text
	add esp, 16
endm

make_text1 proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit1
	cmp eax, 'Z'
	jg make_digit1
	sub eax, 'A'
	lea esi, letters1
	jmp draw_text1
make_digit1:
	cmp eax, '0'
	jl make_space1
	cmp eax, '9'
	jg make_space1
	sub eax, '0'
	lea esi, digits
	jmp draw_text1
make_space1:	
	mov eax, 26 ; de la 0 pana la 25 sunt litere, 26 e space
	lea esi, letters1
	
draw_text1:
	mov ebx, symbol_width1
	mul ebx
	mov ebx, symbol_height1
	mul ebx
	add esi, eax
	mov ecx, symbol_height1
bucla_simbol_linii1:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height1
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width1
bucla_simbol_coloane1:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb1
	mov dword ptr [edi], 0
	jmp simbol_pixel_next1
simbol_pixel_alb1:
	mov dword ptr [edi], 05262ddh
simbol_pixel_next1:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane1
	pop ecx
	loop bucla_simbol_linii1
	popa
	mov esp, ebp
	pop ebp
	ret
make_text1 endp

; un macro ca sa apelam mai usor desenarea simbolului
make_text_macro1 macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_text1
	add esp, 16
endm

; functia de desenare - se apeleaza la fiecare click
; sau la fiecare interval de 200ms in care nu s-a dat click
; arg1 - evt (0 - initializare, 1 - click, 2 - s-a scurs intervalul fara click)
; arg2 - x
; arg3 - y
draw proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1]
	cmp eax, 1
	jz evt_click

	;mai jos e codul care intializeaza fereastra cu pixeli albi
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	push 200
	push area
	call memset
	add esp, 12
	jmp afisare_litere
	
evt_click:

	mov e_x,0
	mov e_0,0
	inc counter
	mov eax,0
	mov edx,0
	mov ax, counter
	div nr
	cmp dx,0
	je x
	cmp dx,1
	je o

continuare:
	
	mov ecx, [ebp+arg2]
	mov ebx, [ebp+arg3]
	
	
	cmp ecx,100
	jl nu_click
	cmp ecx,140
	jg nu_click2
	cmp ebx,100
	jl nu_click
	cmp ebx,140
	jg nu_click4
	cmp e_x,1
	je schimba_litera_x
	cmp e_0,1
	je schimba_litera_0


jmp afisare_litere


nu_click:
jmp afisare_litere

nu_click2:
cmp ecx,180
jg nu_click3
cmp ebx,100
jl nu_click
cmp ebx,140
jg nu_click5
	cmp e_x,1
	je schimba_litera1_x
	cmp e_0,1
	je schimba_litera1_0
jmp afisare_litere

nu_click3:
cmp ecx,220
jg nu_click
cmp ebx,100
jl nu_click
cmp ebx,140
jg nu_click6
	cmp e_x,1
	je schimba_litera2_x
	cmp e_0,1
	je schimba_litera2_0
jmp afisare_litere

nu_click4:
cmp ebx,180
jg nu_click7
	cmp e_x,1
	je schimba_litera3_x
	cmp e_0,1
	je schimba_litera3_0
jmp afisare_litere

nu_click5:
cmp ebx,180
jg nu_click8
	cmp e_x,1
	je schimba_litera4_x
	cmp e_0,1
	je schimba_litera4_0
jmp afisare_litere

nu_click6:
cmp ebx,180
jg nu_click9
	cmp e_x,1
	je schimba_litera5_x
	cmp e_0,1
	je schimba_litera5_0
jmp afisare_litere

nu_click7:
cmp ebx,220
jg nu_click
	cmp e_x,1
	je schimba_litera6_x
	cmp e_0,1
	je schimba_litera6_0
jmp afisare_litere	

nu_click8:
cmp ebx,220
jg nu_click
	cmp e_x,1
	je schimba_litera7_x
	cmp e_0,1
	je schimba_litera7_0
jmp afisare_litere

nu_click9:
cmp ebx,220
jg nu_click
	cmp e_x,1
	je schimba_litera8_x
	cmp e_0,1
	je schimba_litera8_0
jmp afisare_litere

x:
mov e_x,1
jmp continuare

o:
mov e_0,1
jmp continuare

schimba_litera_x:
mov litera,'B'
mov a,1
jmp afisare_litere

schimba_litera_0:
mov litera,'C'
mov a,0
jmp afisare_litere

schimba_litera1_x:
mov litera1,'B'
mov b,1
jmp afisare_litere

schimba_litera1_0:
mov litera1,'C'
mov b,0
jmp afisare_litere

schimba_litera2_x:
mov litera2,'B'
mov c1,1
jmp afisare_litere

schimba_litera2_0:
mov litera2,'C'
mov c1,0
jmp afisare_litere

schimba_litera3_x:
mov d,1
mov litera3,'B'
jmp afisare_litere

schimba_litera3_0:
mov litera3,'C'
mov d,0
jmp afisare_litere

schimba_litera4_x:
mov e,1
mov litera4,'B'
jmp afisare_litere

schimba_litera4_0:
mov litera4,'C'
mov e,0
jmp afisare_litere

schimba_litera5_x:
mov f,1
mov litera5,'B'
jmp afisare_litere

schimba_litera5_0:
mov litera5,'C'
mov f,0
jmp afisare_litere

schimba_litera6_x:
mov g,1
mov litera6,'B'
jmp afisare_litere

schimba_litera6_0:
mov litera6,'C'
mov g,0
jmp afisare_litere

schimba_litera7_x:
mov h,1
mov litera7,'B'
jmp afisare_litere

schimba_litera7_0:
mov litera7,'C'
mov h,0
jmp afisare_litere

schimba_litera8_x:
mov i,1
mov litera8,'B'
jmp afisare_litere

schimba_litera8_0:
mov litera8,'C'
mov i,0
jmp afisare_litere



afisare_litere:
	;scriem un mesaj

	
	make_text_macro litera, area, 100, 100
	make_text_macro litera1, area, 140, 100
	make_text_macro litera2, area, 180, 100


	
	make_text_macro litera3, area, 100, 140
	make_text_macro litera4, area, 140, 140
	make_text_macro litera5, area, 180, 140

	

	make_text_macro litera6, area, 100, 180
	make_text_macro litera7, area, 140, 180
	make_text_macro litera8, area, 180, 180
	
	make_text_macro1 "B",area,500,40
	make_text_macro1 "Y",area,510,40
	make_text_macro1 "D",area,500,60
	make_text_macro1 "R",area,510,60
	make_text_macro1 "A",area,520,60
	make_text_macro1 "G",area,530,60
	make_text_macro1 "O",area,540,60
	make_text_macro1 "T",area,550,60
	make_text_macro1 "A",area,560,60
	make_text_macro1 "M",area,500,80
	make_text_macro1 "I",area,510,80
	make_text_macro1 "R",area,520,80
	make_text_macro1 "C",area,530,80
	make_text_macro1 "E",area,540,80
	make_text_macro1 "A",area,550,80
	make_text_macro1 " ",area,560,80
	
	make_text_macro1 "T",area,200,20
	make_text_macro1 "I",area,210,20 
	make_text_macro1 "C",area,220,20
	make_text_macro1 " ",area,230,20
	make_text_macro1 "T",area,240,20
	make_text_macro1 "A",area,250,20
	make_text_macro1 "C",area,260,20
	make_text_macro1 " ",area,270,20
	make_text_macro1 "T",area,280,20
	make_text_macro1 "O",area,290,20
	make_text_macro1 "E",area,300,20

	

cmp a,1
jne exit1
cmp e,1
jne exit2
cmp i,1
jne exit3
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw;

exit1:
exit2:
exit3:

cmp a,1
jne exit4
cmp d,1
jne exit5
cmp g,1
jne exit6
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw;

exit4:
exit5:
exit6:

cmp a,1
jne exit7
cmp b,1
jne exit8
cmp c1,1
jne exit9
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw;

exit7:
exit8:
exit9:

cmp b,1
jne exit10
cmp e,1
jne exit11
cmp h,1
jne exit12
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

exit10:
exit11:
exit12:

cmp c1,1
jne exit13
cmp e,1
jne exit14
cmp g,1
jne exit15
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

exit13:
exit14:
exit15:

cmp c1,1
jne exit16
cmp f,1
jne exit17
cmp i,1
jne exit18
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

exit16:
exit17:
exit18:

cmp d,1
jne exit19
cmp e,1
jne exit20
cmp f,1
jne exit21
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

exit19:
exit20:
exit21:

cmp g,1
jne exit22
cmp h,1
jne exit23
cmp i,1
jne exit24
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'X', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400

exit22:
exit23:
exit24:

cmp a,0
jne xit1
cmp e,0
jne xit2
cmp i,0
jne xit3
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw;

xit1:
xit2:
xit3:

cmp a,0
jne xit4
cmp d,0
jne xit5
cmp g,0
jne xit6
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw;

xit4:
xit5:
xit6:

cmp a,0
jne xit7
cmp b,0
jne xit8
cmp c1,0
jne xit9
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw;

xit7:
xit8:
xit9:

cmp b,0
jne xit10
cmp e,0
jne xit11
cmp h,0
jne xit12
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

xit10:
xit11:
xit12:

cmp c1,0
jne xit13
cmp e,0
jne xit14
cmp g,0
jne xit15
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

xit13:
xit14:
xit15:

cmp c1,0
jne xit16
cmp f,0
jne xit17
cmp i,0
jne xit18
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

xit16:
xit17:
xit18:

cmp d,0
jne xit19
cmp e,0
jne xit20
cmp f,0
jne xit21
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400
jmp final_draw

xit19:
xit20:
xit21:

cmp g,0
jne xit22
cmp h,0
jne xit23
cmp i,0
jne xit24
make_text_macro1 ' ',area ,320,400
make_text_macro1 'P',area ,330,400
make_text_macro1 'L',area ,340,400
make_text_macro1 'A',area ,350,400
make_text_macro1 'Y',area ,360,400
make_text_macro1 'E',area ,370,400
make_text_macro1 'R',area ,380,400
make_text_macro1 ' ',area ,390,400
make_text_macro1 'O', area, 400, 400
make_text_macro1 ' ', area, 410, 400
make_text_macro1 'W', area, 420, 400
make_text_macro1 'O', area, 430, 400
make_text_macro1 'N', area, 440, 400
make_text_macro1 ' ',area , 450,400

xit22:
xit23:
xit24:

final_draw:
	popa
	mov esp, ebp
	pop ebp
	ret
draw endp
start:
	;alocam memorie pentru zona de desenat
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	call malloc
	add esp, 4
	add eax,400
	mov area, eax
	;apelam functia de desenare a ferestrei
	; typedef void (*DrawFunc)(int evt, int x, int y);
	; void __cdecl BeginDrawing(const char *title, int width, int height, unsigned int *area, DrawFunc draw);
	push offset draw
	push area
	push area_height
	push area_width
	push offset window_title
	call BeginDrawing
	add esp, 20
	
	;terminarea programului
	push 0
	call exit
end start
