package BusinessLogic;
import javax.swing.JTable;

import DataAccess.Querry;

public class Administration {

	public Querry q = new Querry();
	
	public void afisareTable(String comanda,JTable table)
	{
		q.afisareTabel(comanda, table);
	}
	
	public String getCantitate(String nume)
	{
		return q.getCantitate(nume);
	}
	
	public void insert_Order(String orderID,String productName,String customerName,String amount)
	{
		q.insert_Order(orderID, productName, customerName, amount);
	}
	
	public String getPret(String nume)
	{
		return q.getPret(nume);
	}
	
	public String getEmail(String nume)
	{
		return q.getEmail(nume);
	}
	
	public String getID()
	{
		return q.getID();
	}
	
	public void Update_cantitate(String cantitate,String nume)
	{
		q.Update_cantitate(cantitate, nume);
	}
	
	public void Update_Order(String id_order,String numeProdus,String numeClient,String cantitate)
	{
		q.Update_Order(id_order, numeProdus, numeClient, cantitate);
	}
	
	public void Update_oferta(String id_produs,String numeProdus,String cantitate,String pret)
	{
		q.Update_oferta(id_produs, numeProdus, cantitate, pret);
	}
	
	public void insert_Produs(String id_produs,String numeProd,String cantitate,String pret)
	{
		q.insert_Produs(id_produs, numeProd, cantitate, pret);
	}
	
	public void Update_Clienti(String id_client,String numeClient,String email)
	{
		q.Update_Clienti(id_client, numeClient, email);
	}
	
	public void insert_Client(String idCustomer,String numeCustomer,String email)
	{
		q.insert_Client(idCustomer, numeCustomer, email);
	}
	
	public void sterge(String produs,int nr)
	{
		q.sterge(produs, nr);
	}
}
