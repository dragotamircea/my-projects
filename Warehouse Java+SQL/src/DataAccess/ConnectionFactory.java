package DataAccess;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;
import java.sql.Connection;

public class ConnectionFactory {

	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String driver = "com.mysql.jdbc.Driver";
	private static final String url = "jdbc:mysql://localhost:3306/warehouse?autoReconnect=true&useSSL=false";
	private static final String user = "root";
	private static final String pass = "ioan123#";
	private static Connection conn;

	
	private static ConnectionFactory singleInstance =  new ConnectionFactory();
	
	private ConnectionFactory(){
	             try {
	            	 Class.forName(driver);
	             } catch (ClassNotFoundException e) {
	                 e.printStackTrace();
	             }
	         
	}
	
	private static Connection createConnection(){
		
			try {
				conn =DriverManager.getConnection(url,user,pass);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return conn;
	}
	
	public static Connection getConnection(){
		createConnection();
		return conn;
	}
	
	public static void close(Connection connection)
	{
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void close(Statement statement)
	{
		try {
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void close(ResultSet resultSet)
	{
		try {
			resultSet.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
