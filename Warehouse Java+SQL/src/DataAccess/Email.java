package DataAccess;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {

	public static void main(String[] args,String toEmail,String adresa,int pret,int cant) {

		final String username = "imobiliaremircea@gmail.com";
		final String password = "imobiliareMircea2017";
		int total = pret * cant;

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("imobiliaremircea@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(toEmail));
			message.setSubject("Factura Produs");
		
			 // This mail has 2 part, the BODY and the embedded image
	         MimeMultipart multipart = new MimeMultipart("related");

	         // first part (the html)
	         BodyPart messageBodyPart = new MimeBodyPart();
	         String htmlText = "<H1>Factura:</H1> <H2>"+adresa+"</H2>   <H3><b>TOTAL:</b> "+cant+" buc. x "+pret+" => "+total+" <u><b>LEI</b></u>.</H3>  <H4>Va multumim!<H4> <H5><b>SC. Warehouse SRL.</b></H5>";
	         messageBodyPart.setContent(htmlText, "text/html");
	         // add it
	         multipart.addBodyPart(messageBodyPart);

	         // second part (the image)
//	         messageBodyPart = new MimeBodyPart();
//	         DataSource fds = new FileDataSource("E:/Imobiliare/apartament2.jpg");
//	         DataSource fds1 = new FileDataSource("E:/Imobiliare/apartament1.jpg");
//	         messageBodyPart.setDataHandler(new DataHandler(fds));
//	         messageBodyPart.setHeader("Content-ID", "<image>");
//	         messageBodyPart.setDataHandler(new DataHandler(fds1));
//	         messageBodyPart.setHeader("Content-ID", "<image1>");

	         // add image to the multipart
	         multipart.addBodyPart(messageBodyPart);

	         // put everything together
	         message.setContent(multipart);
			
			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
