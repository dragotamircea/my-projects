package DataAccess;

public class Order {
	private int orderID;
	private String productName;
	private String customerName;
	private int amount;
	public Order(int orderID, String productName, String customerName, int amount) {
	
		this.orderID = orderID;
		this.productName = productName;
		this.customerName = customerName;
		this.amount = amount;
	}
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	

}
