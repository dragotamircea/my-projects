package DataAccess;


import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Presentation.Warehouse;


public class Querry {
	
	private ArrayList<Product> produse = new ArrayList<>();
	private ArrayList<Order> comenzi = new ArrayList<>();
	private ArrayList<Customer> clienti = new ArrayList<>();
	private static String findStatementString = "Select * from customer where customerID= ?";
	private static String findStatementString1 = "Select * from customer;";
	
	public static Customer findById(int customerID)
	{
		Customer deReturnat = new Customer(customerID, findStatementString, findStatementString);
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = conn.prepareStatement(findStatementString);
			findStatement.setLong(1,customerID);
			rs = findStatement.executeQuery();
			rs.next();
			
			deReturnat.customerName =rs.getString("customerName");
			deReturnat.email =rs.getString("email");
			
			ConnectionFactory.close(rs);
			ConnectionFactory.close(conn);
			ConnectionFactory.close(findStatement);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return deReturnat;
	}
	
	
	public static ResultSet allCustomers()
	{
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = conn.prepareStatement(findStatementString1);
			rs = findStatement.executeQuery();
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return rs;
	}
	
	
//Reflection
	public <T> DefaultTableModel buildTableModel(ArrayList<T> ob)
	{
		
		  Vector<String> columnNames = new Vector<String>();
		  columnNames=getColumn(ob.get(0));
		  Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	
		  for(Object o:ob)
		  {
			  Vector<Object> vector = new Vector<Object>();
		        for (Field field : o.getClass().getDeclaredFields()) {
		        	field.setAccessible(true);
		            Object value;
		            
		            try {
		            	value = field.get(o);
		            	vector.add(value);
		            } catch (IllegalArgumentException e) {
		                e.printStackTrace();
		            } catch (IllegalAccessException e) {
					
						e.printStackTrace();
					}
		        }
		        data.add(vector);
		  }
		  return new DefaultTableModel(data, columnNames);
	}
	   
	
	
	public static <T> Vector<String> getColumn(T object) {
		 // names of columns
	    Vector<String> columnNames = new Vector<String>();
		
        for (Field field : object.getClass().getDeclaredFields()) {
        		field.setAccessible(true);
        		Object value;
            try {
            	value = field.get(object);
                columnNames.add(field.getName());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return columnNames;
    }


	public void insert_Produs(String id_produs,String numeProd,String cantitate,String pret)
	{
		Connection conn = ConnectionFactory.getConnection();
		Statement stmt= null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(Comenzi.Insert_oferta(id_produs,numeProd,cantitate,pret));
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	 }
	
	public void insert_Client(String idCustomer,String numeCustomer,String email)
	{
		Connection conn = ConnectionFactory.getConnection();
		Statement stmt= null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(Comenzi.Insert_Client(idCustomer,numeCustomer,email));

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	  }
	
	public void insert_Order(String orderID,String productName,String customerName,String amount)
	{
		Connection conn = ConnectionFactory.getConnection();
		Statement stmt= null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(Comenzi.Insert_Order(orderID,productName,customerName,amount));

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public void sterge(String produs,int nr)
	{	 
			 	Connection conn = ConnectionFactory.getConnection();
	            Statement stmt= null;
	    		
	    		try {
	    			stmt = conn.createStatement();
	    			if(nr==1)
	    	         stmt.execute(Comenzi.Delete_Oferta(produs));  
	    	         if(nr==2)
	    	          stmt.execute(Comenzi.Delete_Order(produs));
	    	          if(nr==3)
	    	            stmt.execute(Comenzi.Delete_Client(produs));  

	    		} catch (SQLException e) {
	    			
	    			e.printStackTrace();
	    		}
	        
	}
	
	public void Update_oferta(String id_produs,String numeProdus,String cantitate,String pret)
	{
		Connection conn = ConnectionFactory.getConnection();
        Statement stmt= null;
		
		try {
			stmt = conn.createStatement();
	        stmt.executeUpdate(Comenzi.Update_oferta(id_produs,numeProdus,cantitate,pret));  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public void Update_Clienti(String id_client,String numeClient,String email)
	{
		Connection conn = ConnectionFactory.getConnection();
        Statement stmt= null;
		try {
			stmt = conn.createStatement();
	        stmt.executeUpdate(Comenzi.Update_Clienti(id_client,numeClient,email));  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public void Update_Order(String id_order,String numeProdus,String numeClient,String cantitate)
	{
		Connection conn = ConnectionFactory.getConnection();
        Statement stmt= null;
		try {
			stmt = conn.createStatement();
	        stmt.executeUpdate(Comenzi.Update_Order(id_order,numeProdus,numeClient,cantitate));  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public String getEmail(String nume)
	{
		Connection conn = ConnectionFactory.getConnection();
		Statement stmt=null;
        ResultSet rs= null;
		try {
			stmt = conn.createStatement();
	       rs = stmt.executeQuery(Comenzi.getEmail(nume));  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		try {
			rs.next();
			return rs.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getID()
	{
		Connection conn = ConnectionFactory.getConnection();
		Statement stmt=null;
        ResultSet rs= null;
		try {
			stmt = conn.createStatement();
	       rs = stmt.executeQuery(Comenzi.getID());  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		try {
			rs.next();
			return rs.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getCantitate(String nume)
	{
		Connection conn = ConnectionFactory.getConnection();
		Statement stmt=null;
        ResultSet rs= null;
		try {
			stmt = conn.createStatement();
	       rs = stmt.executeQuery(Comenzi.getCantitate(nume));  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		try {
			rs.next();
			return rs.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getPret(String nume)
	{
		Connection conn = ConnectionFactory.getConnection();
		Statement stmt=null;
        ResultSet rs= null;
		try {
			stmt = conn.createStatement();
	       rs = stmt.executeQuery(Comenzi.getPret(nume));  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		try {
			rs.next();
			return rs.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void Update_cantitate(String cantitate,String nume)
	{
		Connection conn = ConnectionFactory.getConnection();
        Statement stmt= null;
		
		try {
			stmt = conn.createStatement();
	        stmt.executeUpdate(Comenzi.Update_Cantitate(cantitate, nume));  

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public void afisareTabel(String comanda,JTable table)
	{
		
		Connection conn = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			rs=stmt.executeQuery(comanda);
		
			if(Warehouse.Oferta==true)
			{
				produse.clear();
				while(rs.next())
				{
					Product p = new Product(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4));
					produse.add(p);
				}
				table.setModel(buildTableModel(produse));
			}
			else if(Warehouse.Comanda==true)
			{
				comenzi.clear();
				while(rs.next())
				{
					Order o = new Order(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
					comenzi.add(o);
				}
				table.setModel(buildTableModel(comenzi));
			}
			else if(Warehouse.Clienti==true)
			{
				clienti.clear();
				while(rs.next())
				{
					Customer c = new Customer(rs.getInt(1),rs.getString(2),rs.getString(3));
					clienti.add(c);
				}
				table.setModel(buildTableModel(clienti));
			}
			else if(Warehouse.Interogare==true)
			{
				produse.clear();
				while(rs.next())
				{
					Product p = new Product(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4));
					produse.add(p);
				}
				table.setModel(buildTableModel(produse));
			}
			
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	
	}
	
}
