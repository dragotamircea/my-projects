package Presentation;

import java.awt.Color;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;


import BusinessLogic.Administration;
import DataAccess.Comenzi;
import DataAccess.Email;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.ScrollPane;
import javax.swing.JScrollBar;
import java.awt.Scrollbar;
import javax.swing.BoxLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;

public class Table {

	JFrame frmTabel;
	private static JTable table;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmUpdate;
	private JLabel lblCeOfertaDoresti;
	private JLabel lblDenumire;
	private JTextField textField;
	private JButton btnSearch;
	private String tip_pret;
	private JTextField textField_3;
	private JLabel lblProdus;
	private JTextField textField_4;
	private JMenuItem mntmDelete;
	private JTextField textField_1;
	private static Statement stmt;
	private  Administration Adm=new Administration();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args,int nrTabel) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Table window = new Table();
					window.frmTabel.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Table() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTabel = new JFrame();
		frmTabel.setResizable(false);
		frmTabel.getContentPane().setFont(new Font("Tahoma", Font.BOLD, 15));
		frmTabel.setTitle("Tabel");
		frmTabel.setBounds(100, 100, 977, 555);
		frmTabel.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		lblCeOfertaDoresti = new JLabel("Ce oferta doresti ?");
		lblCeOfertaDoresti.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblCeOfertaDoresti.setVisible(false);
		
		lblDenumire = new JLabel("Denumire:");
		lblDenumire.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblDenumire.setVisible(false);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setVisible(false);
		
		btnSearch = new JButton("Search");
		btnSearch.setVisible(false);
		btnSearch.setBackground(new Color(158, 83, 26));
        btnSearch.setForeground(Color.WHITE);
        btnSearch.setFocusPainted(false);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setVisible(false);
		
		JLabel lblEmail = new JLabel("Nume:");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblEmail.setVisible(false);
		
		JButton btnSendDetails = new JButton("BUY");
	
		btnSendDetails.setVisible(false);
		btnSendDetails.setBackground(new Color(158, 83, 26));
        btnSendDetails.setForeground(Color.WHITE);
        btnSendDetails.setFocusPainted(false);
		
		lblProdus = new JLabel("Denumirea Produsului:");
		lblProdus.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblProdus.setVisible(false);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setVisible(false);
		
		JLabel lblCantitate = new JLabel("Cantitatea:");
		lblCantitate.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblCantitate.setVisible(false);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setVisible(false);
		
		GroupLayout groupLayout = new GroupLayout(frmTabel.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(33)
					.addComponent(lblCeOfertaDoresti)
					.addGap(90)
					.addComponent(lblDenumire)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 355, Short.MAX_VALUE)
					.addComponent(btnSearch)
					.addGap(77))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 947, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(24)
					.addComponent(lblEmail)
					.addGap(18)
					.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(lblProdus)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
					.addGap(26)
					.addComponent(lblCantitate)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
					.addComponent(btnSendDetails, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
					.addGap(48))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCeOfertaDoresti, GroupLayout.PREFERRED_SIZE, 17, Short.MAX_VALUE)
						.addComponent(lblDenumire)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSearch))
					.addGap(53)
					.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 307, GroupLayout.PREFERRED_SIZE)
					.addGap(48)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEmail)
						.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSendDetails)
						.addComponent(lblCantitate)
						.addComponent(lblProdus)
						.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(20))
		);
		
		table = new JTable();
		scrollPane_1.setViewportView(table);
		frmTabel.getContentPane().setLayout(groupLayout);
		
		menuBar = new JMenuBar();
		frmTabel.setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("Edit");
		menuBar.add(mnNewMenu);
		
		mntmUpdate = new JMenuItem("Update");
		
		if(Warehouse.Interogare==true)
		{
			menuBar.setVisible(false);
			if(Warehouse.nrInterogare==1){
			frmTabel.setTitle("Search Oferte");
			lblCeOfertaDoresti.setVisible(true);
			lblDenumire.setVisible(true);
			textField.setVisible(true);
			btnSearch.setVisible(true);
			textField_3.setVisible(true);
			lblEmail.setVisible(true);
			btnSendDetails.setVisible(true);
			lblProdus.setVisible(true);
			textField_4.setVisible(true);
			lblCantitate.setVisible(true);
			textField_1.setVisible(true);
			
			btnSearch.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(textField.getText().isEmpty())
					{
					Adm.afisareTable(Comenzi.afisareTabelOferta(),table);
					}
					else
					Adm.afisareTable(Comenzi.Interogare1(textField.getText()),table);
				}
			});
			
			btnSendDetails.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					
					if(Integer.parseInt(Adm.getCantitate(textField_4.getText())) >= Integer.parseInt(textField_1.getText()))
					{
					Adm.insert_Order(Adm.getID(), textField_4.getText(), textField_3.getText(), textField_1.getText());
					Adm.Update_cantitate(Integer.toString(Integer.parseInt(Adm.getCantitate(textField_4.getText()))-Integer.parseInt(textField_1.getText())), textField_4.getText());
					int pret = Integer.parseInt(Adm.getPret(textField_4.getText()));
					int cant = Integer.parseInt(textField_1.getText());
					
					Email.main(null,Adm.getEmail(textField_3.getText()),textField_4.getText(),pret,cant);
					}
					else
					JOptionPane.showMessageDialog(frmTabel, "Cantitate prea mare !", "Eroare", JOptionPane.ERROR_MESSAGE);

				}
			});
			}
			
		}
		mntmUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Warehouse.Oferta == true)
				{
					Update.main(null);
				}
				else if(Warehouse.Comanda == true)
				{
					Update3.main(null);
				}
				else if(Warehouse.Clienti==true)
				{
					Update2.main(null);
				}
				
			}
		});
		mnNewMenu.add(mntmUpdate);
		
		mntmDelete = new JMenuItem("Delete");
		mntmDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Warehouse.Oferta == true)
				{
					Delete.main(null);
				}
				else if(Warehouse.Comanda == true)
				{
					Delete.main(null);
				}
				else if(Warehouse.Clienti==true)
				{
					Delete.main(null);
				}
				
			
			}
		});
		mnNewMenu.add(mntmDelete);
		
	}
	
	public JTable getTable()
	{
		return table;
	}
	}
