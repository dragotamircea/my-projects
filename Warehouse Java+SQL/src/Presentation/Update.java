package Presentation;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;


import javax.swing.JToggleButton;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import BusinessLogic.Administration;
import DataAccess.Comenzi;
import DataAccess.ConnectionFactory;

import javax.swing.event.AncestorEvent;

public class Update{

	private JFrame frmUpdate;
	public static String nrProd;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_5;
	private Administration Adm = new Administration();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Update window = new Update();
					window.frmUpdate.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Update() {
		initialize();
	}
	
	public static void afisareNume(JComboBox comboBox)
	{
		Connection conn = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rs=stmt.executeQuery(Comenzi.id_oferta());
			while(rs.next())
			{
				comboBox.addItem(rs.getString(1));
			}
	
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmUpdate = new JFrame();
		frmUpdate.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 17));
		frmUpdate.setTitle("Update");
		frmUpdate.setBounds(100, 100, 504, 354);
		frmUpdate.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Alege"}));
		afisareNume(comboBox);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nrProd = (String) comboBox.getSelectedItem();
				
			}
		});
		
		JLabel lblIdAgentie = new JLabel("Nume Produs:");
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JLabel lblIdSpatiu = new JLabel("Cantitate:");
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		JLabel lblVanzare = new JLabel("Pret:");
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		JLabel lblSchmbat = new JLabel("Schimbat!");
		lblSchmbat.setVisible(false);
		
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setBackground(new Color(158, 83, 26));
        btnUpdate.setForeground(Color.WHITE);
        btnUpdate.setFocusPainted(false);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Adm.Update_oferta((String) comboBox.getSelectedItem(), textField.getText(), textField_1.getText(), textField_2.getText());
				lblSchmbat.setVisible(true);
				Warehouse.inchide(650, lblSchmbat);
			}
		});
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.setBackground(new Color(158, 83, 26));
        btnInsert.setForeground(Color.WHITE);
        btnInsert.setFocusPainted(false);
		
		JLabel lblInsert = new JLabel("Adaugat!");
		
		JLabel lblIdNouaOferta = new JLabel("ID Produs Nou:");
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		
		lblInsert.setVisible(false);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Adm.insert_Produs(textField_5.getText(), textField.getText(), textField_1.getText(), textField_2.getText());
			
				lblInsert.setVisible(true);
				Warehouse.inchide(650, lblInsert);
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(frmUpdate.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(27)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblVanzare)
						.addComponent(lblIdSpatiu)
						.addComponent(lblIdAgentie)
						.addComponent(comboBox, 0, 104, Short.MAX_VALUE)
						.addComponent(textField, 0, 0, Short.MAX_VALUE)
						.addComponent(textField_1, 0, 0, Short.MAX_VALUE)
						.addComponent(textField_2, 0, 0, Short.MAX_VALUE))
					.addGap(175)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblIdNouaOferta))
							.addGap(63))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addComponent(lblSchmbat))
								.addComponent(btnUpdate)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
									.addComponent(btnInsert)
									.addComponent(lblInsert)))
							.addGap(94))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(40, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblIdNouaOferta))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIdAgentie)
						.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblIdSpatiu)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblVanzare))
						.addComponent(lblInsert))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnInsert))
					.addGap(16)
					.addComponent(lblSchmbat)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnUpdate)
					.addGap(7))
		);
		frmUpdate.getContentPane().setLayout(groupLayout);
	}
}
