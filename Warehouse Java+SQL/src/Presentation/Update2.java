package Presentation;

import java.awt.Color;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import BusinessLogic.Administration;
import DataAccess.Comenzi;
import DataAccess.ConnectionFactory;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Update2 {

	private JFrame frame;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField;
	private Administration Adm = new Administration();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Update2 window = new Update2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Update2() {
		initialize();
	}
	
	public static void afisareNume(JComboBox comboBox)
	{
		
		Connection conn = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rs=stmt.executeQuery(Comenzi.id_customer());
			while(rs.next())
			{
				comboBox.addItem(rs.getString(1));
			}
	
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JLabel lblDenumire = new JLabel("Nume:");
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		JLabel lblCaracteristici = new JLabel("Email:");
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setBackground(new Color(158, 83, 26));
        btnUpdate.setForeground(Color.WHITE);
        btnUpdate.setFocusPainted(false);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Alege"}));
		afisareNume(comboBox);
		
		JLabel lblSchimbat = new JLabel("Schimbat!");
		lblSchimbat.setVisible(false);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Adm.Update_Clienti((String) comboBox.getSelectedItem(),textField_1.getText(),textField_2.getText());
				lblSchimbat.setVisible(true);
				Warehouse.inchide(650, lblSchimbat);
			}
		});
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JLabel lblIdTipNou = new JLabel("ID Client nou:");
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.setBackground(new Color(158, 83, 26));
        btnInsert.setForeground(Color.WHITE);
        btnInsert.setFocusPainted(false);
		
		JLabel lblAdaugat = new JLabel("Adaugat!");
		lblAdaugat.setVisible(false);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Adm.insert_Client(textField.getText(),textField_1.getText(),textField_2.getText());
				lblAdaugat.setVisible(true);
				Warehouse.inchide(650, lblAdaugat);
			}
		});
		
		
		
		
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(353, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDenumire)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCaracteristici)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(52)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblIdTipNou)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAdaugat)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblSchimbat)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
							.addGap(26))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addGap(202)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnUpdate)
								.addComponent(btnInsert))
							.addGap(29))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblDenumire)
								.addComponent(lblIdTipNou))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblCaracteristici)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblAdaugat)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnInsert)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblSchimbat)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnUpdate)
					.addContainerGap(18, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
