package Presentation;

import java.awt.Color;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import BusinessLogic.Administration;
import DataAccess.Comenzi;
import DataAccess.ConnectionFactory;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Update3{

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private Administration Adm = new Administration();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Update3 window = new Update3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void afisareNume(JComboBox comboBox)
	{
		Connection conn = ConnectionFactory.getConnection();
		ResultSet rs = null;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			rs=stmt.executeQuery(Comenzi.id_order());
			while(rs.next())
			{
				comboBox.addItem(rs.getString(1));
			}
	
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

	/**
	 * Create the application.
	 */
	public Update3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 472, 314);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Alege"}));
		afisareNume(comboBox);
		
		JLabel lblNewLabel = new JLabel("Nume Produs:");
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nume Client:");
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		JLabel lblSuprafata = new JLabel("Cantitate:");
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		
		JLabel lblIdTip = new JLabel("ID Comanda Noua:");
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		
		final JLabel lblSchimbat = new JLabel("Schimbat!");
		lblSchimbat.setVisible(false);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setBackground(new Color(158, 83, 26));
        btnUpdate.setForeground(Color.WHITE);
        btnUpdate.setFocusPainted(false);
		
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Adm.Update_Order((String) comboBox.getSelectedItem(),textField.getText(),textField_1.getText(),textField_2.getText());
				lblSchimbat.setVisible(true);
				Warehouse.inchide(650, lblSchimbat);
			}
		});
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.setBackground(new Color(158, 83, 26));
        btnInsert.setForeground(Color.WHITE);
        btnInsert.setFocusPainted(false);
		
		JLabel lblAdaugat = new JLabel("Adaugat!");
		lblAdaugat.setVisible(false);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Adm.insert_Order(textField_3.getText(), textField.getText(), textField_1.getText(), textField_2.getText());
				Adm.Update_cantitate(Integer.toString(Integer.parseInt(Adm.getCantitate(textField.getText()))-Integer.parseInt(textField_2.getText())), textField.getText());
				lblAdaugat.setVisible(true);
				Warehouse.inchide(650, lblAdaugat);
			}
		});
		
		;
		
	
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(25)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblSuprafata)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAdaugat)
								.addComponent(btnUpdate)
								.addComponent(btnInsert)
								.addComponent(lblSchimbat))
							.addGap(7))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(35)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblIdTip))
							.addGap(140)))
					.addGap(69))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(25)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(lblIdTip))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(lblNewLabel_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSuprafata)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(28, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(119)
					.addComponent(lblAdaugat)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnInsert)
					.addGap(18)
					.addComponent(lblSchimbat)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnUpdate)
					.addGap(20))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
