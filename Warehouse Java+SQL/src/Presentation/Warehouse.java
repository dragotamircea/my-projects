package Presentation;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import BusinessLogic.Administration;
import DataAccess.Comenzi;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.Color;
import javax.swing.JCheckBoxMenuItem;
import java.awt.List;
import java.awt.Choice;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import java.awt.Font;

public class Warehouse {

	private JFrame frmSistemeImobiliare;
	protected static int nrInterogare;
	public static boolean Oferta = false;
	public static boolean Comanda= false;
	public static boolean Clienti = false;
	public static boolean Interogare = false;
	private Administration Adm = new Administration();
	
	/**
	 * Launch the application
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Warehouse window = new Warehouse();
					
					window.frmSistemeImobiliare.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Warehouse() {
		initialize();
	}

	public static void inchide(int delay,JLabel label)
	{
		   ActionListener taskPerformer = new ActionListener() {
		       public void actionPerformed(ActionEvent evt) {
		    	   label.setVisible(false);
		       }
		   };
		    new Timer(delay, taskPerformer).start();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSistemeImobiliare = new JFrame();
		frmSistemeImobiliare.setResizable(false);
		frmSistemeImobiliare.setTitle("Sistem Magazin");
		frmSistemeImobiliare.setBounds(100, 100, 1056, 521);
		frmSistemeImobiliare.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File("C:/Users/User/Desktop/Construction2.jpg"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		JPanel panel_1 = new JPanelWithBackground(image);
		GroupLayout groupLayout = new GroupLayout(frmSistemeImobiliare.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 982, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 453, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(36, Short.MAX_VALUE))
		);
		
		JLabel lblImobiliare = new JLabel("WareHouse");
		lblImobiliare.setFont(new Font("Showcard Gothic", Font.BOLD, 18));
		
		JButton btnNewButton = new JButton("Oferte");
		 btnNewButton.setBackground(new Color(158, 83, 26));
	        btnNewButton.setForeground(Color.WHITE);
	        btnNewButton.setFocusPainted(false);
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = true;
				Comanda = false;
				Clienti = false;
				Interogare = false;
				Table t = new Table();
				t.frmTabel.setVisible(true);
				Adm.afisareTable(Comenzi.afisareTabelOferta(),t.getTable());
			}
		});
		
		JButton btnNewButton_1 = new JButton("Clienti");
		btnNewButton_1.setBackground(new Color(158, 83, 26));
        btnNewButton_1.setForeground(Color.WHITE);
        btnNewButton_1.setFocusPainted(false);
		
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = false;
				Clienti = true;
				Interogare = false;
				Table t = new Table();
				t.frmTabel.setVisible(true);
				Adm.afisareTable(Comenzi.afisareTabelClienti(),t.getTable());
			}
		});
		
		JButton btnNewButton_2 = new JButton("Comenzi");
		btnNewButton_2.setBackground(new Color(158, 83, 26));
        btnNewButton_2.setForeground(Color.WHITE);
        btnNewButton_2.setFocusPainted(false);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Oferta = false;
				Comanda = true;
				Clienti = false;
				Interogare = false;
				Table t = new Table();
				t.frmTabel.setVisible(true);
				Adm.afisareTable(Comenzi.afisareTabelOrder(),t.getTable());
			}
		});
		
		JButton btnCumpara = new JButton("Cumpara");
		btnCumpara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = false;
				Clienti = false;
				Interogare = true;
				nrInterogare =1;
				Table t = new Table();
				t.frmTabel.setVisible(true);
			}
		});
		btnCumpara.setForeground(Color.WHITE);
		btnCumpara.setFocusPainted(false);
		btnCumpara.setBackground(new Color(158, 83, 26));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(397)
					.addComponent(lblImobiliare)
					.addContainerGap(533, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(btnNewButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnNewButton_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnNewButton_2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnCumpara, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(954, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(64)
					.addComponent(lblImobiliare)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_1)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_2)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnCumpara, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(218, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		frmSistemeImobiliare.getContentPane().setLayout(groupLayout);
		
		JMenuBar menuBar = new JMenuBar();
		frmSistemeImobiliare.setJMenuBar(menuBar);
		
		JMenu mnOferte = new JMenu("Oferte");
		menuBar.add(mnOferte);
		
		JMenuItem mntmVeziOferte = new JMenuItem("Vezi Oferte");
		mntmVeziOferte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Oferta = true;
				Comanda = false;
				Clienti = false;
				Interogare = false;
				Table t = new Table();
				t.frmTabel.setVisible(true);
				Adm.afisareTable(Comenzi.afisareTabelOferta(),t.getTable());
			}
		});
		mnOferte.add(mntmVeziOferte);
		
		JMenu mnModifica = new JMenu("Modifica");
		mnOferte.add(mnModifica);
		
		JMenuItem mntmUpdate = new JMenuItem("Update");
		mntmUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = true;
				Comanda = false;
				Clienti = false;
				Interogare = false;
				Update.main(null);
			}
		});
		mnModifica.add(mntmUpdate);
		
		JMenuItem mntmSterge = new JMenuItem("Sterge");
		mntmSterge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = true;
				Comanda = false;
				Clienti = false;
				Interogare = false;
				Delete.main(null);
			}
		});
		mnModifica.add(mntmSterge);
		
		JMenu mnTipuri = new JMenu("Clienti");
		menuBar.add(mnTipuri);
		
		JMenuItem mntmVeziClienti= new JMenuItem("Vezi Clienti");
		mntmVeziClienti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = false;
				Clienti = true;
				Interogare = false;
				Table t = new Table();
				t.frmTabel.setVisible(true);
				Adm.afisareTable(Comenzi.afisareTabelClienti(),t.getTable());
			}
		});
		mnTipuri.add(mntmVeziClienti);
		
		JMenu mnModifica_3 = new JMenu("Modifica");
		mnTipuri.add(mnModifica_3);
		
		JMenuItem mntmUpdate_3 = new JMenuItem("Update");
		mntmUpdate_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = false;
				Clienti = true;
				Interogare = false;
				Update2.main(null);
				
			}
		});
		mnModifica_3.add(mntmUpdate_3);
		
		JMenuItem mntmSterge_3 = new JMenuItem("Sterge");
		mntmSterge_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = false;
				Clienti = true;
				Interogare = false;
				Delete.main(null);
			}
		});
		mnModifica_3.add(mntmSterge_3);
		
		JMenu mnNewMenu_1 = new JMenu("Comenzi");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmVeziSpatiile = new JMenuItem("Vezi Comenzile");
		mntmVeziSpatiile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = true;
				Clienti = false;
				Interogare = false;
				Table t = new Table();
				t.frmTabel.setVisible(true);
				Adm.afisareTable(Comenzi.afisareTabelOrder(),t.getTable());
			}
		});
		mnNewMenu_1.add(mntmVeziSpatiile);
		
		JMenu mnModifica_2 = new JMenu("Modifica");
		mnNewMenu_1.add(mnModifica_2);
		
		JMenuItem mntmUpdate_2 = new JMenuItem("Update");
		mntmUpdate_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = true;
				Clienti = false;
				Interogare = false;
				Update3.main(null);
			}
		});
		mnModifica_2.add(mntmUpdate_2);
		
		JMenuItem mntmSterge_2 = new JMenuItem("Sterge");
		mntmSterge_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = true;
				Clienti = false;
				Interogare = false;
				Delete.main(null);
			}
		});
		mnModifica_2.add(mntmSterge_2);
		
		JMenu mnInterogari = new JMenu("Interogari");
		menuBar.add(mnInterogari);
		
		JMenuItem mntmOferteCuApartament = new JMenuItem("Search Oferte");
		mntmOferteCuApartament.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Oferta = false;
				Comanda = false;
				Clienti = false;
				Interogare = true;
				nrInterogare =1;
				Table t = new Table();
				t.frmTabel.setVisible(true);
				
			}
		});
		mnInterogari.add(mntmOferteCuApartament);
	}
}
